package forms;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;

public class SessionForm {

    private static final String DATE_REGEXP = "(0[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012])/((19|20)\\d\\d)";
    private static final String START_SESSION_REGEXP = "[\\d]{2}\\:[\\d]{2}";
    @NotBlank
    private String hall;

    @NotBlank
    private String film;

    @Pattern(regexp = DATE_REGEXP, message = "{sessionForm.date.incorrect}")
    private String startDate;
    @Pattern(regexp = DATE_REGEXP, message = "{sessionForm.date.incorrect}")
    private String endDate;
    @Pattern(regexp = START_SESSION_REGEXP, message = "{sessionForm.startSession.incorrect}")
    private String startSession;

    public String getHall() {
        return hall;
    }

    public void setHall(String hall) {
        this.hall = hall;
    }

    public String getFilm() {
        return film;
    }

    public void setFilm(String film) {
        this.film = film;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartSession() {
        return startSession;
    }

    public void setStartSession(String startSession) {
        this.startSession = startSession;
    }

}
