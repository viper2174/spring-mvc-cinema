package forms;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class FilmForm{

    @NotBlank(message = "{filmForm.nameNotBlank}")
    private String name;
    @Min(value = 0, message = "{filmForm.hours.atLeastZero}")
    @Max(value = 24, message = "{filmForm.minutes.moreThanTwentyFour}")
    private int hours;
    @Min(value = 0, message = "{filmForm.minutes.atLeastZero}")
    @Max(value = 60, message = "{filmForm.minutes.lessThanSixty}")
    private int minutes;
    @Min(value = 0, message = "{filmForm.seconds.atLeastZero}")
    @Max(value = 60, message = "{filmForm.seconds.lessThanSixty}")
    private int seconds;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

}
