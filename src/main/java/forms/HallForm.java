package forms;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

public class HallForm {

    @Pattern(regexp = "[А-Яа-я\\w]+", message = "{hallForm.name.regexp}")
    private String name;
    @Min(value = 1, message = "{hallForm.rows.atLeastOne}")
    private int rows;
    @Min(value = 1, message = "{hallForm.places.atLeastOne}")
    private int places;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }
}
