package forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

public class RegistrationForm {

    @NotEmpty(message="{registrationForm.login.notEmpty}")
    @Size(min = 4, message = "{registrationForm.login.atLeastFour}")
    private String login;

    @NotEmpty(message="{registrationForm.password.notEmpty}")
    @Pattern(regexp = "[\\w\\d]+", message = "{registrationForm.password.regexp}")
    private String password;

    @NotEmpty(message="{registrationForm.name.notEmpty}")
    @Size(min=2, max=30, message = "{registrationForm.name.betweenTwoAndThirty}")
    private String name;

    @NotEmpty(message="{registrationForm.surname.notEmpty}")
    @Size(min=2, max=30, message = "{registrationForm.surname.betweenTwoAndThirty}")
    private String surname;

    @NotEmpty(message="{registrationForm.email.notEmpty}")
    @Email(message = "{registrationForm.email.regexp}")
    private String email;

    @NotNull
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @Past(message = "{registrationForm.birthday.past}")
    private Date birthday;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
}
