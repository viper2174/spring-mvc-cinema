package forms;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LoginForm {

    @Size(min = 4, message = "{loginForm.login.sizeAtLeastFour}")
    private String login;
    @Pattern(regexp = "[\\w\\d]+", message = "{loginForm.password.regexp}")
    private String password;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
