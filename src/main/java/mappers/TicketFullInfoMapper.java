package mappers;

import entities.beans.TicketFullInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketFullInfoMapper implements RowMapper<TicketFullInfo> {

    @Override
    public TicketFullInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        String filmName = resultSet.getString("f.name");
        int hour = resultSet.getInt("f.hours");
        int minute = resultSet.getInt("f.minutes");
        int second = resultSet.getInt("f.seconds");
        String startTime = resultSet.getTimestamp("s.start_time").toString();
        int row = resultSet.getInt("p.row");
        int place = resultSet.getInt("p.place");
        String hallName = resultSet.getString("h.name");
        String status = resultSet.getString("t.status");
        String userName = resultSet.getString("u.name");
        String userSurname = resultSet.getString("u.surname");
        return new TicketFullInfo(filmName, hour, minute, second
                , startTime, row, place, hallName, status, userName, userSurname);
    }
}
