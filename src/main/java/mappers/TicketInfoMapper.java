package mappers;

import entities.beans.TicketInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketInfoMapper implements RowMapper<TicketInfo> {

    @Override
    public TicketInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("t.id");
        int row = resultSet.getInt("p.row");
        int place = resultSet.getInt("p.place");
        String status = resultSet.getString("t.status");
        return new TicketInfo(id, row, place, status);
    }
}
