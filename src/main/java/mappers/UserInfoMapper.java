package mappers;

import entities.beans.UserInfo;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserInfoMapper implements RowMapper<UserInfo> {

    @Override
    public UserInfo mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("users.id");
        String login = resultSet.getString("users.login");
        String password = resultSet.getString("users.password");
        String name = resultSet.getString("users.name");
        String surname = resultSet.getString("users.surname");
        String roleName = resultSet.getString("roles.name");
        String email = resultSet.getString("users.email");
        Date birthday = resultSet.getDate("users.birthday");
        String status = resultSet.getString("status");
        return new UserInfo(id, login, password, name, surname, birthday, email, roleName, status);
    }
}
