package mappers;

import entities.User;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        String login = resultSet.getString("login");
        String password = resultSet.getString("password");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        Date birthday = resultSet.getDate("birthday");
        String email = resultSet.getString("email");
        int roleId = resultSet.getInt("role_id");
        String status = resultSet.getString("status");
        return new User(login, name, surname, birthday, roleId, email, password, status);
    }
}
