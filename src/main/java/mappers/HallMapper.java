package mappers;

import entities.Hall;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HallMapper implements RowMapper<Hall> {

    @Override
    public Hall mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int rows = resultSet.getInt("rows");
        int places = resultSet.getInt("places");
        return new Hall(id, name, rows, places);
    }
}
