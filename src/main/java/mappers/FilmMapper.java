package mappers;

import entities.Film;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmMapper implements RowMapper<Film> {

    @Override
    public Film mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        int hours = resultSet.getInt("hours");
        int minutes = resultSet.getInt("minutes");
        int seconds = resultSet.getInt("seconds");
        return new Film(id, name, hours, minutes, seconds);
    }
}
