package entities.beans;

public class SessionBean {

    private int id;
    private String hallName;
    private String filmName;
    private String startTime;
    private String endTime;

    public SessionBean(int id, String hallName, String filmName, String startTime
            , String endTime) {
        this.id = id;
        this.hallName = hallName;
        this.filmName = filmName;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
}
