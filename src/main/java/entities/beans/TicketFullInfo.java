package entities.beans;

public class TicketFullInfo {

    private String filmName;
    private int hour;
    private int minute;
    private int second;
    private String startTime;
    private int row;
    private int place;
    private String hallName;
    private String status;
    private String userName;
    private String userSurname;

    public TicketFullInfo(String filmName, int hour, int minute, int second
            , String startTime, int row, int place, String hallName, String status
            , String userName, String userSurname) {
        this.filmName = filmName;
        this.hour = hour;
        this.minute = minute;
        this.second = second;
        this.startTime = startTime;
        this.row = row;
        this.place = place;
        this.hallName = hallName;
        this.status = status;
        this.userName = userName;
        this.userSurname = userSurname;
    }

    public String getFilmName() {
        return filmName;
    }

    public void setFilmName(String filmName) {
        this.filmName = filmName;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserSurname() {
        return userSurname;
    }

    public void setUserSurname(String userSurname) {
        this.userSurname = userSurname;
    }

    @Override
    public String toString() {
        return "TicketFullInfo{" +
                "filmName='" + filmName + '\'' +
                ", hour=" + hour +
                ", minute=" + minute +
                ", second=" + second +
                ", startTime='" + startTime + '\'' +
                ", row=" + row +
                ", place=" + place +
                ", hallName='" + hallName + '\'' +
                ", status='" + status + '\'' +
                ", userName='" + userName + '\'' +
                ", userSurname='" + userSurname + '\'' +
                '}';
    }
}
