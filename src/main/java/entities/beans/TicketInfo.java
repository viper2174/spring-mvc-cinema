package entities.beans;

public class TicketInfo {

    private int id;
    private int row;
    private int place;
    private String status;

    public TicketInfo(int id, int row, int place, String status) {
        this.id = id;
        this.row = row;
        this.place = place;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getPlace() {
        return place;
    }

    public void setPlace(int place) {
        this.place = place;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "TicketInfo{" +
                "id=" + id +
                ", row=" + row +
                ", place=" + place +
                ", status='" + status + '\'' +
                '}';
    }
}
