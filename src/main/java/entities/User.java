package entities;

import java.util.Date;

public class User {

    private String login;
    private String name;
    private String surname;
    private Date birthday;
    private int roleId;
    private String email;
    private String password;
    private String status;

    public User(String login, String name, String surname, Date birthday
            , int roleId, String email, String password, String status) {
        this.login = login;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.roleId = roleId;
        this.email = email;
        this.password = password;
        this.status = status;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
