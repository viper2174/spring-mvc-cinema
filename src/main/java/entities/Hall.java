package entities;

public class Hall {

    private int id;
    private String name;
    private int rows;
    private int places;

    public Hall() {
    }

    public Hall(int id, String name, int rows, int places) {
        this.id = id;
        this.name = name;
        this.rows = rows;
        this.places = places;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getPlaces() {
        return places;
    }

    public void setPlaces(int places) {
        this.places = places;
    }

    /* need to override toString in this way to correctly show name */
    @Override
    public String toString() {
        return name;
    }
}