package controllers;

import config.UserInfoSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;

@RequestMapping("logout")
@Controller
public class LogoutController {

    @Autowired
    private UserInfoSession userInfoSession;

    @RequestMapping(method = RequestMethod.GET)
    private String logout () {
        userInfoSession.removeUserFromSession();
        return "redirect:/login";
    }
}
