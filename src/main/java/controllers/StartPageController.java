package controllers;

import config.UserInfoSession;
import entities.beans.UserInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import services.interfaces.UserService;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("")
public class StartPageController {

    @Autowired
    private UserInfoSession userInfoSession;

    @Autowired
    private UserService userService;

    @GetMapping
    private String getMainPage() {
        return "mainPage";
    }

    @GetMapping("/welcome")
    private String getWelcomePage(HttpSession httpSession) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        httpSession.setAttribute("userInfo", userInfo);
        return "welcome";
    }
}
