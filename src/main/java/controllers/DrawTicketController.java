package controllers;

import com.google.zxing.WriterException;
import entities.beans.TicketFullInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.interfaces.PictureService;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
@RequestMapping("/draw")
public class DrawTicketController {

    private static final String IMG_CONTENT_TYPE = "image/png";
    private static final int WIDTH = 100;
    private static final int HEIGHT = 50;

    @Autowired
    private PictureService pictureService;

    @RequestMapping(value = "/drawTicket", method = RequestMethod.GET)
    public void qrCode(HttpSession httpSession, HttpServletResponse response) throws IOException, WriterException {
        response.setContentType(IMG_CONTENT_TYPE);
        TicketFullInfo ticketFullInfo = (TicketFullInfo) httpSession.getAttribute("ticketInfo");
        response.getOutputStream().write(pictureService.getZXhelper(ticketFullInfo, WIDTH, HEIGHT));
        response.getOutputStream().flush();
        response.getOutputStream().close();
    }
}
