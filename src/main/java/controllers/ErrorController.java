package controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping ("error")
public class ErrorController {

    @RequestMapping(method = RequestMethod.GET)
    private String showErrorPage (){
        return "error";
    }
}
