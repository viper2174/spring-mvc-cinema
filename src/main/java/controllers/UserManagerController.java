package controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import services.interfaces.UserService;

@Controller
@RequestMapping("/admin/userManager")
public class UserManagerController {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private UserService userService;

    @GetMapping
    private String getUserManagerPage (Model model){
        model.addAttribute("users", userService.getUsersWithoutAdmin());
        return "admin/userManager";
    }

    @PostMapping(value = "/changeUserStatus")
    private @ResponseBody String changeUserStatus (@RequestParam("userName") String login) {
        logger.info(String.format("user status %s is going to changed", login));
        userService.changeUserStatus(login);
        return userService.getUserByLogin(login).getStatus();
    }
}
