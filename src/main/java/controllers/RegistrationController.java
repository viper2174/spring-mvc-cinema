package controllers;

import entities.User;
import forms.RegistrationForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.interfaces.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.Date;

@Controller
@RequestMapping("/registration")
public class RegistrationController {

    private static final int USER_ROLE_ID = 2;
    private static final String USER_STATUS_UNBLOCKED = "unblocked";
    private static final String REGISTRATION_FAILED_MESSAGE = "registration failed due to internal issues";
    private static final String MODEL_ATTRIBUTE_REGISTRATION = "registration";

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String openLoginPage(Model model) {
        model.addAttribute(MODEL_ATTRIBUTE_REGISTRATION, new RegistrationForm());
        return "registration";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String registerUser(@Valid @ModelAttribute(MODEL_ATTRIBUTE_REGISTRATION)
                            RegistrationForm registrationForm, BindingResult bindingResult,
                            HttpSession httpSession) {
        if (bindingResult.hasErrors()) {
            return "registration";
        } else {
            return getRedirectPathAndRegisterUser(registrationForm, httpSession);
        }
    }

    private String getRedirectPathAndRegisterUser(RegistrationForm registrationForm, HttpSession httpSession) {
        Date birthday = new Date(registrationForm.getBirthday().getTime());
        User user = new User(registrationForm.getLogin(), registrationForm.getName(), registrationForm.getSurname(),
                birthday, USER_ROLE_ID, registrationForm.getEmail(), registrationForm.getPassword(),
                USER_STATUS_UNBLOCKED);
        if (userService.registerUser(user)) {
            return "redirect:/login";
        }
        httpSession.setAttribute("error", REGISTRATION_FAILED_MESSAGE);
        return "redirect:/error";
    }

}
