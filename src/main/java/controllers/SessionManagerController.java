package controllers;

import com.google.gson.Gson;
import entities.beans.SessionBean;
import entities.beans.SessionInfo;
import forms.SessionForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import services.interfaces.FilmService;
import services.interfaces.HallService;
import services.interfaces.SessionService;

import java.util.List;

@Controller
@RequestMapping("/admin/sessionManager")
public class SessionManagerController {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private FilmService filmService;

    @Autowired
    private HallService hallService;

    @Autowired
    private SessionService sessionService;

    @GetMapping
    private String getSessionManagerPage(Model model) {
        List<SessionBean> sessionBeans = sessionService.getSessions();
        model.addAttribute("sessions", sessionBeans);
        model.addAttribute("films", filmService.getFilms());
        model.addAttribute("halls", hallService.getHalls());
        model.addAttribute("sessionForm", new SessionForm());
        return "admin/sessionManager";
    }

    @PostMapping(value = "/addSession")
    private String addSession (@ModelAttribute("sessionForm")SessionForm sessionForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.info("session Form data contains invalid data");
            return "redirect:/admin/sessionManager";
        } else {
            sessionService.addSession(sessionForm);
            return "redirect:/admin/sessionManager";
        }
    }

    @GetMapping(value = "/getFilms", produces = "application/json")
    private @ResponseBody String getFilms(@RequestParam(value = "hallName")String hallName) {
        List<SessionInfo> sessions = sessionService.getSessionsByHall(hallName);
        String json = new Gson().toJson(sessions);
        return json;
    }

    @GetMapping(value = "/deleteSession{sessionId}")
    private String deleteHall(@PathVariable(value = "sessionId") String sessionId) {
        logger.info(String.format("session with id %s is going to be deleted", sessionId));
        sessionService.deleteSession(sessionId);
        return "redirect:/admin/sessionManager";
    }
}
