package controllers;

import config.UserInfoSession;
import entities.beans.TicketFullInfo;
import entities.beans.UserInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import services.interfaces.PictureService;
import services.interfaces.TicketService;
import services.interfaces.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoSession userInfoSession;

    @RequestMapping(method = RequestMethod.GET)
    public String openProfilePage(HttpSession session, Model model) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        session.setAttribute("userInfo", userInfo);
        List<TicketFullInfo> bookedTickets = ticketService.getBookedTickets(userInfo.getId());
        List<TicketFullInfo> boughtTickets = ticketService.getBoughtTickets(userInfo.getId());
        model.addAttribute("bookedTickets", bookedTickets);
        model.addAttribute("boughtTickets", boughtTickets);
        return "user/profile";
    }

    @RequestMapping(value="/uploadAvatar", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public String uploadAvatar(@RequestParam("file") MultipartFile file, HttpSession session) {
        pictureService.uploadAvatar (file, (UserInfo)session.getAttribute("userInfo"));
        return "redirect:/profile";
    }

    @RequestMapping(value="/showAvatar", method = RequestMethod.GET)
    @ResponseBody
    public byte[] showAvatar(HttpSession session) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        return pictureService.getAvatar(userInfo);
    }

    @RequestMapping(value="/changePassword", method = RequestMethod.POST)
    public String changePassword(@RequestParam("passwordOld") String passwordOld,
                                 @RequestParam("passwordNew") String passwordNew,
                                 @RequestParam("passwordConfirm") String passwordConfirm,
                                 HttpSession httpSession) {
        userService.changePassword (passwordOld, passwordNew
                , passwordConfirm, (UserInfo) httpSession.getAttribute("userInfo"));
        return "redirect:/profile";
    }

}
