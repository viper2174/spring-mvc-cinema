package controllers;

import forms.FilmForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import services.interfaces.FilmService;
import services.interfaces.PictureService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/filmManager")
public class FilmManagerController {

    private static final String FILM_FORM_INVALID = "information for adding film info is invalid";
    private static final String UPDATE_FILM_INFO_INVALID = "information for updating film info is invalid";
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private FilmService filmService;

    @Autowired
    private PictureService pictureService;

    @GetMapping
    private String getFilmManagerPage(Model model) {
        model.addAttribute("films", filmService.getFilms());
        return "admin/filmManager";
    }

    @GetMapping(value = "/addNewFilm")
    private String addFilmPage(Model model) {
        model.addAttribute("filmForm", new FilmForm());
        return "admin/addNewFilm";
    }

    @PostMapping(value = "/addFilm", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    private ModelAndView addFilm (@Valid @ModelAttribute("filmForm") FilmForm filmForm
            , BindingResult bindingResult, @RequestParam("file") MultipartFile file, Model model) {
        if (bindingResult.hasErrors()) {
            logger.info(FILM_FORM_INVALID);
            return new ModelAndView("admin/addNewFilm");
        } else {
            pictureService.uploadFilmImg(file, filmForm.getName());
            filmService.addFilm(filmForm);
            return new ModelAndView("redirect:/admin/filmManager");
        }
    }

    @GetMapping(value = "/deleteFilm{filmId}")
    private String deleteFilm(@PathVariable(value = "filmId") String filmId) {
        logger.info(String.format("film with id %s is going to be deleted", filmId));
        filmService.deleteFilm(filmId);
        return "redirect:/admin/filmManager";
    }

    @GetMapping(value = "/updateFilmPage{filmId}")
    private String updateFilmPage(@PathVariable(value = "filmId") String filmId, Model model) {
        logger.info(String.format("film with id %s is going to be updated", filmId));
        model.addAttribute("updatedFilm", filmService.getFilmById(filmId));
        model.addAttribute("filmUpdateForm", new FilmForm());
        return "admin/updateFilm";
    }

    @PostMapping(value = "/updateFilm{filmId}")
    private String updateFilm(@Valid @ModelAttribute("filmUpdateForm") FilmForm filmForm,
                              BindingResult bindingResult, @PathVariable(value = "filmId") String filmId) {
        if (bindingResult.hasErrors()) {
            logger.info(UPDATE_FILM_INFO_INVALID);
            return "admin/updateFilm";
        } else {
            filmService.updateFilm(filmForm, filmId);
            return "redirect:/admin/filmManager";
        }
    }
}
