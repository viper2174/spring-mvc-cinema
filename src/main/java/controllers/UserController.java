package controllers;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping ("/user")
public class UserController {

    private final Log logger = LogFactory.getLog(this.getClass());

    @RequestMapping (method = RequestMethod.GET)
    private String showUserPage (){
        return "user/user";
    }

}
