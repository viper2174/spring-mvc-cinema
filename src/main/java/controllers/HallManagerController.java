package controllers;

import forms.HallForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import services.interfaces.HallService;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/hallManager")
public class HallManagerController {

    private static final String HALL_INFO_INVALID = "information for adding hall info is invalid";
    private static final String HALL_UPDATE_INFO_INVALID = "information for updating hall info is invalid";
    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private HallService hallService;

    @GetMapping
    private String getHallManagerPage(Model model) {
        model.addAttribute("halls", hallService.getHalls());
        return "admin/hallManager";
    }

    @GetMapping(value = "/addNewHall")
    private String addHallPage(Model model) {
        model.addAttribute("hallForm", new HallForm());
        return "admin/addNewHall";
    }

    @PostMapping(value = "/addHall")
    private String addHall
            (@Valid @ModelAttribute("hallForm") HallForm hallForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            logger.info(HALL_INFO_INVALID);
            return "admin/addNewHall";
        } else {
            hallService.addHall(hallForm);
            return "redirect:/admin/hallManager";
        }
    }

    @GetMapping(value = "/deleteHall{hallId}")
    private String deleteHall(@PathVariable(value = "hallId") String hallId) {
        logger.info(String.format("hall with id %s is going to be deleted", hallId));
        hallService.deleteHall(hallId);
        return "redirect:/admin/hallManager";
    }

    @GetMapping(value = "/updateHallPage{hallId}")
    private String updateHallPage(@PathVariable(value = "hallId") String hallId, Model model) {
        logger.info(String.format("hall with id %s is going to be updated", hallId));
        model.addAttribute("updatedHall", hallService.getHallById(hallId));
        model.addAttribute("hallUpdateForm", new HallForm());
        return "admin/updateHall";
    }

    @PostMapping(value = "/updateHall{hallId}")
    private String updateHall(@Valid @ModelAttribute("hallUpdateForm") HallForm hallForm,
                              BindingResult bindingResult, @PathVariable(value = "hallId") String hallId) {
        if (bindingResult.hasErrors()) {
            logger.info(HALL_UPDATE_INFO_INVALID);
            return "admin/updateHall";
        } else {
            hallService.updateHall(hallForm, hallId);
            return "redirect:/admin/hallManager";
        }
    }
}
