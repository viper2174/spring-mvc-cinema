package controllers;

import config.UserInfoSession;
import entities.Film;
import entities.beans.SessionBean;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import services.interfaces.*;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user/bookTicket")
public class BookTicketController {

    private final Log logger = LogFactory.getLog(this.getClass());

    @Autowired
    private FilmService filmService;

    @Autowired
    private SessionService sessionService;

    @Autowired
    private PictureService pictureService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoSession userInfoSession;

    @RequestMapping(method = RequestMethod.GET)
    private ModelAndView showContactsPage (){
        return new ModelAndView("user/bookTicket")
                .addObject("films", filmService.getFilms());
    }

    @RequestMapping(value="/showFilmImg{filmName}", method = RequestMethod.GET)
    @ResponseBody
    private byte[] showFilmPoster(@PathVariable(value = "filmName") String filmName) {
        return pictureService.getFilmPicture(filmName);
    }

    @RequestMapping(value = "/getSessionsByFilm{filmId}")
    private ModelAndView showListOfSessionsForSpecifiedFilm(@PathVariable(value = "filmId") String filmId){
        List<SessionBean> sessionBeans = sessionService.getSessionsByFilmId(filmId);
        Film film = filmService.getFilmById(filmId);
        return new ModelAndView("user/showSessions")
                .addObject("sessions", sessionBeans)
                .addObject("film", film);
    }

    @RequestMapping(value = "/openHall{sessionId}")
    private ModelAndView openHallForSpecifiedSession
            (@PathVariable(value = "sessionId") String sessionId){
        ModelAndView modelAndView = new ModelAndView("user/hallMap");
        List<TicketInfo> ticketInfoList =
                ticketService.getTicketsBySessionId(Integer.parseInt(sessionId));
        ticketService.sortTickets(ticketInfoList);
        SessionBean sessionBean = sessionService.getSession(sessionId);
        modelAndView.addObject("tickets", ticketInfoList);
        int maxRow = ticketInfoList.stream().mapToInt(TicketInfo::getRow).max().getAsInt();
        modelAndView.addObject("maxRow", maxRow);
        modelAndView.addObject("sessionBean", sessionBean);
        return modelAndView;
    }

    @RequestMapping(value = "/determineChairColorForUser{status}", method = RequestMethod.GET)
    @ResponseBody
    private byte[] getChairPicture (@PathVariable(value = "status") String status) {
        return pictureService.getChairColorForUser(status);
    }

    @RequestMapping(value="/bookTicket",method=RequestMethod.POST)
    public String  bookTicketForUser
            (@RequestParam(value="choosed[]") List<Integer> checked) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        ticketService.bookTickets(checked, userInfo);
        return "/profile";
    }

    @RequestMapping(value="/buyTicket",method=RequestMethod.POST)
    public String  buyTicketForUser
            (@RequestParam(value="choosed[]") List<Integer> checked) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        ticketService.buyTickets(checked, userInfo);
        return "/profile";
    }
}
