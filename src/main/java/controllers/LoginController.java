package controllers;

import config.UserInfoSession;
import entities.beans.UserInfo;
import forms.LoginForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import services.interfaces.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

@Controller
@RequestMapping("/login")
public class LoginController {

    private final Log logger = LogFactory.getLog(this.getClass());
    private static final String BLOCKED = "blocked";
    private static final String LOGIN_FORM = "loginForm";
    private static final String LOGIN = "login";
    private static final String USER_INFO = "userInfo";

    @Autowired
    private UserService userService;

    @Autowired
    private UserInfoSession userInfoSession;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @RequestMapping(method = RequestMethod.GET)
    public String openLoginPage(Model model) {
        model.addAttribute(LOGIN_FORM, new LoginForm());
        return "login";
    }

    @RequestMapping(value="/check", method = RequestMethod.POST)
    public String checkUser(@ModelAttribute(LOGIN_FORM) @Valid LoginForm loginForm
            , BindingResult bindingResult, HttpSession session, Model model) {
        if (bindingResult.hasErrors()) {
            logger.info("login form contains invalid data");
            return LOGIN;
        } else {
            return checkLogin (loginForm, session, model);
        }
    }

    private String checkLogin(LoginForm loginForm, HttpSession session, Model model) {
        UserInfo userInfo = userInfoSession.getUserFromSession(userService);
        if (userInfo != null && passwordIsCorrect(loginForm, userInfo)) {
            session.setAttribute(USER_INFO, userInfo);
            return getRedirectPath(userInfo, model);
        } else {
            logger.info("password is incorrect");
            return "redirect:/login";
        }
    }

    private String getRedirectPath(UserInfo userInfo, Model model) {
        if (userInfo.getStatus() == BLOCKED) {
            model.addAttribute("error", String.format("your account %s is blocked.", userInfo.getLogin()));
            return "redirect:/error";
        }
        if ("user".equals(userInfo.getRoleName())) {
            return "redirect:/user";
        } else if ("admin".equals(userInfo.getRoleName())) {
            return "redirect:/admin";
        } else {
            return "redirect:/error";
        }
    }

    private boolean passwordIsCorrect(LoginForm loginForm, UserInfo userInfo) {
        return passwordEncoder.matches(loginForm.getPassword(), userInfo.getPassword());
    }

}
