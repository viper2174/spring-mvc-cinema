package services.implementation;

import dao.interfaces.HallDAO;
import dao.interfaces.PlacesDAO;
import entities.Hall;
import forms.HallForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import services.interfaces.HallService;

import java.util.List;

@Service
public class HallServiceImpl implements HallService {

    @Autowired
    private HallDAO hallDAO;

    @Autowired
    private PlacesDAO placesDAO;

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public boolean addHall(HallForm hallForm) {
        boolean isHallAdded = hallDAO.addHall(hallForm);
        Hall addedHall = hallDAO.getHallByName(hallForm.getName());
        boolean arePlacesCreated = placesDAO.addPlaces(addedHall);
        return isHallAdded && arePlacesCreated;
    }

    @Override
    public List<Hall> getHalls() {
        return hallDAO.getHalls();
    }

    @Override
    public boolean deleteHall(String hallId) {
        boolean isHallDeleted = hallDAO.deleteHall(hallId);
        boolean arePlacesDeleted = placesDAO.deletePlaces (hallId);
        return isHallDeleted && arePlacesDeleted;
    }

    @Override
    public Hall getHallById(String hallId) {
        return hallDAO.getHallById(hallId);
    }

    @Override
    public boolean updateHall(HallForm hallForm, String hallId) {
        boolean isHallUpdated = hallDAO.updateHall (hallForm, hallId);
        boolean arePlacesUpdated = placesDAO.updatePlaces (hallForm, hallId);
        return isHallUpdated && arePlacesUpdated;
    }
}
