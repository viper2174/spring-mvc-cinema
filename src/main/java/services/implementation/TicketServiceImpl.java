package services.implementation;

import dao.interfaces.TicketDAO;
import entities.beans.TicketFullInfo;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.interfaces.TicketService;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    private TicketDAO ticketDAO;

    @Override
    public List<TicketInfo> getTicketsBySessionId(int sessionId) {
        return ticketDAO.getTicketsBySessionId(sessionId);
    }

    @Override
    public void sortTickets(List<TicketInfo> ticketInfoList) {
        ticketInfoList.sort((o1, o2) -> {
            if (o1.getRow() > o2.getRow()) {
                return 1;
            }
            if (o1.getRow() < o2.getRow()) {
                return -1;
            }
            if (o1.getPlace() < o2.getPlace()) {
                return 1;
            }
            if (o1.getPlace() > o2.getPlace()) {
                return -1;
            }
            return 0;
        });
    }

    @Override
    public void bookTickets(List<Integer> checked, UserInfo userInfo) {
        ticketDAO.bookTickets(checked, userInfo);
    }

    @Override
    public void buyTickets(List<Integer> checked, UserInfo userInfo) {
        ticketDAO.buyTickets(checked, userInfo);
    }

    @Override
    public List<TicketFullInfo> getBookedTickets(int userId) {
        return ticketDAO.getBookedTickets(userId);
    }

    @Override
    public List<TicketFullInfo> getBoughtTickets(int userId) {
        return ticketDAO.getBoughtTickets(userId);
    }
}
