package services.implementation;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import entities.beans.TicketFullInfo;
import entities.beans.UserInfo;
import net.glxn.qrgen.QRCode;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import services.interfaces.PictureService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;

@Service
public class PictureServiceImpl implements PictureService {

    private static final String AVATAR_FOLDER = "src/main/webapp/resources/img/avatars";
    private static final String FILM_FOLDER = "src/main/webapp/resources/img/films";
    private static final String FREE_ICON_PATH = "src/main/webapp/resources/img/grey.png";
    private static final String BOUGHT_ICON_PATH = "src/main/webapp/resources/img/red.png";
    private static final String NO_AVATAR_FILM_PATH = "src/main/webapp/resources/img/questionMark.png";
    private static final String DEFAULT_AVATAR_PATH = "src/main/webapp/resources/img/questionMark.png";

    private final Log logger = LogFactory.getLog(this.getClass());

    @Override
    public void uploadAvatar(MultipartFile file, UserInfo userInfo) {
        if (userInfo != null && file != null && !file.isEmpty()) {
            File folder = new File(AVATAR_FOLDER);
            folder.mkdir();
            String destinationFileName = new StringBuilder().append("/")
                    .append(userInfo.getLogin()).append(".png").toString();
            File avatar = new File(folder + destinationFileName);
            saveImg(file, avatar);
        }
    }

    private void saveImg(MultipartFile file, File filename) {
        try {
            filename.createNewFile();
            file.transferTo(filename);
        } catch (IOException e) {
            logger.error("IO Exception in saveImg()", e);
        }
    }

    @Override
    public byte[] getAvatar(UserInfo userInfo) {
        if (userInfo != null) {
            return getUserAvatar(userInfo);
        } else return null;
    }

    private byte[] getUserAvatar(UserInfo userInfo) {
        String avatarFileName = new StringBuilder().append("/")
                .append(userInfo.getLogin()).append(".png").toString();
        byte[] customAvatar = getUserCustomAvatar(avatarFileName);
        if (customAvatar != null) {
            return customAvatar;
        } else {
            return getDefaultAvatar();
        }
    }


    private byte[] getUserCustomAvatar(String avatarFileName) {
        try {
            return Files.readAllBytes(new File(AVATAR_FOLDER + avatarFileName).toPath());
        } catch (IOException e) {
            logger.error("IO Exception in getUserCustomAvatar()", e);
        }
        return null;
    }


    private byte[] getDefaultAvatar() {
        try {
            return Files.readAllBytes(new File(DEFAULT_AVATAR_PATH).toPath());
        } catch (IOException e) {
            logger.error("IO Exception in getDefaultAvatar()", e);
        }
        return null;
    }

    @Override
    public byte[] getFilmPicture(String filmName) {
        String filmPath = getFilmPath(filmName);
        try {
            return Files.readAllBytes(new File(filmPath).toPath());
        } catch (IOException e) {
            logger.error("IO Exception in getFilmPicture()", e);
        }
        return null;
    }

    private String getFilmPath(String filmName) {
        String filmFileName = new StringBuilder().append("/")
                .append(filmName).append(".png").toString();
        String testedPath = FILM_FOLDER + filmFileName;
        if (Files.exists(Paths.get(testedPath))) {
            return testedPath;
        } else {
            return NO_AVATAR_FILM_PATH;
        }
    }

    @Override
    public void uploadFilmImg(MultipartFile file, String name) {
        if (!file.isEmpty()) {
            File folder = new File(FILM_FOLDER);
            folder.mkdir();
            String destinationFileName = new StringBuilder().append("/").append(name).append(".png").toString();
            File poster = new File(folder + destinationFileName);
            saveImg(file, poster);
        }
    }

    @Override
    public byte[] getChairColorForUser(String status) {
        try {
            return getChairPictureByStatus(status);
        } catch (IOException e) {
            logger.error("IO Exception in getChairColorForUser()", e);
        }
        return null;
    }

    @Override
    public byte[] drawTicket(TicketFullInfo ticketFullInfo) {
        return QRCode.from(ticketFullInfo.toString()).stream().toByteArray();
    }

    private byte[] getChairPictureByStatus(String status) throws IOException {
        if ("free".equals(status)) {
            return Files.readAllBytes(new File(FREE_ICON_PATH).toPath());
        } else if ("booked".equals(status)) {
            return Files.readAllBytes(new File(BOUGHT_ICON_PATH).toPath());
        } else if ("bought".equals(status)) {
            return Files.readAllBytes(new File(BOUGHT_ICON_PATH).toPath());
        } else {
            return null;
        }
    }

    public byte[] getZXhelper(TicketFullInfo ticketFullInfo, int width, int height) {
        try {
            return getQrCode(ticketFullInfo, width, height);
        } catch (WriterException e) {
            logger.error("Writer exception appeared");
        } catch (IOException e) {
            logger.error("IOException appeared");
        }
        return null;
    }

    private byte[] getQrCode(TicketFullInfo ticketFullInfo, int width, int height) throws IOException, WriterException {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode
                    (ticketFullInfo.toString(), BarcodeFormat.QR_CODE, width, height);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "png", byteArrayOutputStream);
            return byteArrayOutputStream.toByteArray();
    }
}
