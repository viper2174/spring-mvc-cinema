package services.implementation;

import dao.interfaces.UserDAO;
import entities.User;
import entities.beans.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.interfaces.UserService;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public boolean registerUser(User user) {
        return userDAO.registerUser(user);
    }

    @Override
    public UserInfo getUserInfo(String login) {
        return userDAO.getUserInfo(login);
    }

    @Override
    public User getUserByLogin(String login) {
        return userDAO.getUserByLogin(login);
    }

    @Override
    public boolean changePassword(String passwordOld, String passwordNew, String passwordConfirm, UserInfo userInfo) {
        return userDAO.updateUserPassword(passwordOld, passwordNew, passwordConfirm, userInfo);
    }

    @Override
    public List<User> getUsers() {
        return userDAO.getUsers();
    }

    @Override
    public List<User> getUsersWithoutAdmin() {
        return userDAO.getUsersWithoutAdmin();
    }

    @Override
    public boolean changeUserStatus(String login) {
        return userDAO.changeUserStatus (login);
    }
}
