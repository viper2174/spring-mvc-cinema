package services.implementation;

import dao.interfaces.FilmDAO;
import dao.interfaces.HallDAO;
import dao.interfaces.SessionDAO;
import entities.beans.SessionBean;
import entities.beans.SessionInfo;
import forms.SessionForm;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import services.interfaces.SessionService;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class SessionServiceImpl implements SessionService {

    @Autowired
    private SessionDAO sessionDAO;

    @Autowired
    private FilmDAO filmDAO;

    @Autowired
    private HallDAO hallDAO;

    @Override
    public List<SessionInfo> getSessionsByHall(String hallName) {
        return sessionDAO.getSessionsByHall(hallName);
    }

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean addSession(SessionForm sessionForm) {
        boolean sessionsAddedCounter = false;
        int filmId = filmDAO.getFilmByName(sessionForm.getFilm()).getId();
        int hallId = hallDAO.getHallByName(sessionForm.getHall()).getId();
        LocalDateTime start = getStartDate (sessionForm);
        LocalDateTime end = getEndDate (sessionForm);
        while (!end.isBefore(start)){
            sessionsAddedCounter = sessionDAO.addSession(filmId, hallId, start);
            start = start.plusDays(1);
        }
        return sessionsAddedCounter;
    }

    private LocalDateTime getStartDate(SessionForm sessionForm) {
        LocalDate startDate = LocalDate.parse
                (sessionForm.getStartDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        LocalTime localTime = LocalTime.parse(sessionForm.getStartSession(), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(startDate, localTime);
    }

    private LocalDateTime getEndDate(SessionForm sessionForm) {
        LocalDate endDate = LocalDate.parse
                (sessionForm.getEndDate(), DateTimeFormatter.ofPattern("MM/dd/yyyy"));
        LocalTime localTime = LocalTime.parse(sessionForm.getStartSession(), DateTimeFormatter.ofPattern("HH:mm"));
        return LocalDateTime.of(endDate,localTime);
    }

    @Override
    public List<SessionBean> getSessions() {
        return sessionDAO.getSessions();
    }

    @Override
    public boolean deleteSession(String sessionId) {
        return sessionDAO.deleteSession(sessionId);
    }

    @Override
    public List<SessionBean> getSessionsByFilmId(String filmId) {
        return sessionDAO.getSessionsByFilmId(filmId);
    }

    @Override
    public SessionBean getSession(String sessionId) {
        return sessionDAO.getSession(sessionId);
    }
}
