package services.implementation;

import dao.interfaces.FilmDAO;
import entities.Film;
import forms.FilmForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import services.interfaces.FilmService;

import java.util.List;

@Service
public class FilmServiceImpl implements FilmService {

    @Autowired
    private FilmDAO filmDAO;

    @Override
    public boolean addFilm(FilmForm filmForm) {
        return filmDAO.addFilm(filmForm);
    }

    @Override
    public List<Film> getFilms() {
        return filmDAO.getFilms();
    }

    @Override
    public boolean deleteFilm(String filmId) {
        return filmDAO.deleteFilm(filmId);
    }

    @Override
    public Film getFilmById(String filmId) {
        return filmDAO.getFilmById(filmId);
    }

    @Override
    public boolean updateFilm(FilmForm filmForm, String filmId) {
        return filmDAO.updateFilm (filmForm, filmId);
    }

}
