package services.interfaces;

import entities.beans.SessionBean;
import entities.beans.SessionInfo;
import forms.SessionForm;

import java.util.List;

public interface SessionService {
    List<SessionInfo> getSessionsByHall(String filmId);
    boolean addSession(SessionForm sessionForm);
    List<SessionBean> getSessions();
    boolean deleteSession(String sessionId);
    List<SessionBean> getSessionsByFilmId(String filmId);
    SessionBean getSession(String sessionId);
}
