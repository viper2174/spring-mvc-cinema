package services.interfaces;

import entities.beans.TicketFullInfo;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;

import java.util.List;

public interface TicketService {
    List<TicketInfo> getTicketsBySessionId(int sessionId);
    void sortTickets(List<TicketInfo> ticketInfoList);
    void bookTickets(List<Integer> checked, UserInfo userInfo);
    void buyTickets(List<Integer> checked, UserInfo userInfo);
    List<TicketFullInfo> getBookedTickets(int id);
    List<TicketFullInfo> getBoughtTickets(int id);
}
