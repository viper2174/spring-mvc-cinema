package services.interfaces;

import entities.Film;
import forms.FilmForm;

import java.util.List;

public interface FilmService {

    List<Film> getFilms();
    boolean addFilm(FilmForm filmForm);
    boolean deleteFilm(String filmId);
    Film getFilmById(String filmId);
    boolean updateFilm(FilmForm filmForm, String filmId);
}
