package services.interfaces;

import entities.Hall;
import forms.HallForm;

import java.util.List;

public interface HallService {

    boolean addHall (HallForm hallForm);

    List<Hall> getHalls();

    boolean deleteHall(String hallId);

    Hall getHallById(String hallId);

    boolean updateHall(HallForm hallForm, String hallId);
}
