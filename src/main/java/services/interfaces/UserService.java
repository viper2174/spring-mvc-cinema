package services.interfaces;

import entities.Role;
import entities.User;
import entities.beans.UserInfo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {

    boolean registerUser (User user);

    UserInfo getUserInfo(String login);

    User getUserByLogin(String login);
    boolean changePassword(String passwordOld, String passwordNew
            , String passwordConfirm, UserInfo userInfo);
    List<User> getUsers();
    List<User> getUsersWithoutAdmin();
    boolean changeUserStatus(String login);
}
