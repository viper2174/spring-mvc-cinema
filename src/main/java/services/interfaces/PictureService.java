package services.interfaces;

import entities.beans.TicketFullInfo;
import entities.beans.UserInfo;
import org.springframework.web.multipart.MultipartFile;

public interface PictureService {

    void uploadAvatar (MultipartFile file, UserInfo userInfo);
    byte[] getAvatar (UserInfo user);
    byte[] getFilmPicture(String filmName);
    void uploadFilmImg(MultipartFile file, String name);
    byte[] getChairColorForUser(String status);
    byte[] drawTicket(TicketFullInfo ticketFullInfo);
    byte[] getZXhelper(TicketFullInfo ticketFullInfo, int i, int i1);
}
