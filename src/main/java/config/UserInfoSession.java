package config;

import entities.beans.UserInfo;
import org.hibernate.annotations.common.util.impl.LoggerFactory;
import org.jboss.logging.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import services.interfaces.UserService;

@Component
@Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class UserInfoSession {

    private UserInfo userInfo;

    public UserInfo getUserFromSession(UserService userService) {
        if (SecurityContextHolder.getContext().getAuthentication().isAuthenticated()) {
            userInfo = userService.getUserInfo(SecurityContextHolder.getContext().getAuthentication().getName());
        }
        return userInfo;
    }

    public void removeUserFromSession () {
        userInfo = null;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }
}
