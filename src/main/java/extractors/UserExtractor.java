package extractors;

import entities.User;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserExtractor implements ResultSetExtractor<User> {

    @Override
    public User extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (resultSet.next()) {
            String login = resultSet.getString("login");
            String password = resultSet.getString("password");
            String name = resultSet.getString("name");
            String surname = resultSet.getString("surname");
            Date birthday = resultSet.getDate("birthday");
            String email = resultSet.getString("email");
            int roleId = resultSet.getInt("role_id");
            String status = resultSet.getString("status");
            return new User(login, name, surname, birthday, roleId, email, password, status);
        } else {
            return null;
        }
    }
}
