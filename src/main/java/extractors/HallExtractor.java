package extractors;

import entities.Hall;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class HallExtractor implements ResultSetExtractor<Hall> {

    @Override
    public Hall extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int rows = resultSet.getInt("rows");
            int places = resultSet.getInt("places");
            return new Hall(id, name, rows, places);
        } else {
            return null;
        }
    }
}
