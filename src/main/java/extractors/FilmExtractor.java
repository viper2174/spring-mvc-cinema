package extractors;

import entities.Film;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;

public class FilmExtractor implements ResultSetExtractor<Film> {
    @Override
    public Film extractData(ResultSet resultSet) throws SQLException, DataAccessException {
        if (resultSet.next()) {
            int id = resultSet.getInt("id");
            String name = resultSet.getString("name");
            int hours = resultSet.getInt("hours");
            int minutes = resultSet.getInt("minutes");
            int seconds = resultSet.getInt("seconds");
            return new Film(id, name, hours, minutes, seconds);
        } else {
            return null;
        }
    }
}
