package dao.implementation;

import dao.interfaces.UserDAO;
import entities.User;
import entities.beans.UserInfo;
import extractors.UserExtractor;
import extractors.UserInfoExtractor;
import mappers.UserMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import java.util.Calendar;
import java.util.List;

@Repository
public class MySQLUserDao implements UserDAO {

    private static final String MINUS = "-";
    private static final int MONTH_SQL_ERROR_PLUS_ONE = 1;
    private static final int UPDATED_ROWS = 1;
    private static final String GET_USERS_WITHOUT_ADMIN = "select * from users where name != 'admin'";
    private static final String USER_INFO_QUERY_BY_LOGIN =
            "select * from users inner join roles on users.role_id = roles.id where users.login=?";
    private static final String INSERT_USER = "insert into users " +
            "(login, password, name, surname, birthday, email, role_id, status) values (?,?,?,?,?,?,?,'unblocked')";
    private static final String GET_USER_BY_LOGIN
            = "select * from users where login=?";
    private static final String UPDATE_USER_PASSWORD = "UPDATE users " +
            "SET password = ? WHERE login = ?";
    private static final String GET_USERS = "select * from users";
    private static final String UPDATE_STATUS =
            "update users set status = ? where login = ?";
    private static final String BLOCKED = "blocked";
    private static final String UNBLOCKED = "unblocked";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserInfo getUserInfo(String login) {
        return jdbcTemplate.query(USER_INFO_QUERY_BY_LOGIN, new UserInfoExtractor(), login);
    }

    @Override
    public boolean registerUser(User user) {
        return jdbcTemplate.update(INSERT_USER, user.getLogin(), passwordEncoder.encode(user.getPassword())
                , user.getName(), user.getSurname(), getDateAsSQL(user), user.getEmail(), user.getRoleId()) == 1;
    }

    private String getDateAsSQL(User user) {
        StringBuilder stringBuilder = new StringBuilder();
        Calendar cal = Calendar.getInstance();
        cal.setTime(user.getBirthday());
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        int year = cal.get(Calendar.YEAR);
        stringBuilder.append(year).append(MINUS).append(month + MONTH_SQL_ERROR_PLUS_ONE).append(MINUS).append(day);
        return stringBuilder.toString();
    }

    @Override
    public User getUserByLogin(String login) {
        return jdbcTemplate.query(GET_USER_BY_LOGIN, new UserExtractor(), login);
    }

    @Override
    public boolean updateUserPassword(String passwordOld, String passwordNew, String passwordConfirm, UserInfo userInfo) {
        User user = getUserByLogin(userInfo.getLogin());
        if (passwordEncoder.matches(passwordOld, user.getPassword()) && passwordNew.equals(passwordConfirm)) {
            return jdbcTemplate.update(UPDATE_USER_PASSWORD, passwordEncoder.encode(passwordNew), userInfo.getLogin()) == 1;
        } else {
            return false;
        }
    }

    @Override
    public List<User> getUsers() {
        return jdbcTemplate.query(GET_USERS, new UserMapper());
    }

    @Override
    public List<User> getUsersWithoutAdmin() {
        return jdbcTemplate.query(GET_USERS_WITHOUT_ADMIN, new UserMapper());    }

    @Override
    public boolean changeUserStatus(String login) {
        User user = getUserByLogin(login);
        if ("unblocked".equals(user.getStatus())) {
            return jdbcTemplate.update(UPDATE_STATUS, BLOCKED, login) == UPDATED_ROWS;
        } else {
            return jdbcTemplate.update(UPDATE_STATUS, UNBLOCKED, login) == UPDATED_ROWS;
        }
    }

    @Override
    public UserInfo getUserInfoByLogin(String login) {
        return jdbcTemplate.query(USER_INFO_QUERY_BY_LOGIN, new UserInfoExtractor(), login);
    }
}
