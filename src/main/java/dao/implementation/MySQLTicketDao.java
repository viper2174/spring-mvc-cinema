package dao.implementation;

import dao.interfaces.TicketDAO;
import dao.interfaces.UserDAO;
import entities.beans.TicketFullInfo;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;
import mappers.TicketFullInfoMapper;
import mappers.TicketInfoMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class MySQLTicketDao implements TicketDAO {

    private static final String GET_TICKETS_BY_SESSION_ID = "select t.id, t.status, p.row, p.place " +
            "from tickets t inner join places p on t.place_id = p.id where t.session_id = ?";
    private static final String BOOK_TICKET_QUERY = "update tickets set status='booked' where id=?";
    private static final String INSERT_INTO_BOOKED = "insert into booked (ticket_id, user_id) value (?,?)";
    private static final String BUY_TICKET_QUERY = "update tickets set status='bought' where id=?";
    private static final String INSERT_INTO_BOUGHT = "insert into bought (ticket_id, user_id) value (?,?)";
    private static final String GET_BOOKED_TICKETS = "select s.start_time, f.name, f.hours, f.minutes," +
            " f.seconds, p.row, p.place, h.name, t.status, u.name, u.surname from booked b " +
            "inner join users u on b.user_id = u.id inner join tickets t on b.ticket_id = t.id " +
            "inner join sessions s on t.session_id = s.id inner join films f on s.film_id = f.id " +
            "inner join halls h on s.hall_id = h.id inner join places p on t.place_id = p.id where u.id = ?";
    private static final String GET_BOUGHT_TICKETS = "select s.start_time, f.name, f.hours, f.minutes," +
            " f.seconds, p.row, p.place, h.name, t.status, u.name, u.surname from bought b " +
            "inner join users u on b.user_id = u.id inner join tickets t on b.ticket_id = t.id " +
            "inner join sessions s on t.session_id = s.id inner join films f on s.film_id = f.id " +
            "inner join halls h on s.hall_id = h.id inner join places p on t.place_id = p.id where u.id = ?";

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<TicketInfo> getTicketsBySessionId(int sessionId) {
        return jdbcTemplate.query(GET_TICKETS_BY_SESSION_ID, new TicketInfoMapper(), sessionId);
    }

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    @Override
    public void bookTickets(List<Integer> checked, UserInfo userInfo) {
        for (Integer id : checked) {
            jdbcTemplate.update(BOOK_TICKET_QUERY, id);
            jdbcTemplate.update(INSERT_INTO_BOOKED, id, userInfo.getId());
        }
    }

    @Override
    public void buyTickets(List<Integer> checked, UserInfo userInfo) {
        for (Integer id : checked) {
            jdbcTemplate.update(BUY_TICKET_QUERY, id);
            jdbcTemplate.update(INSERT_INTO_BOUGHT, id, userInfo.getId());
        }
    }

    @Override
    public List<TicketFullInfo> getBookedTickets(int userId) {
        return jdbcTemplate.query(GET_BOOKED_TICKETS, new TicketFullInfoMapper(), userId);
    }

    @Override
    public List<TicketFullInfo> getBoughtTickets(int userId) {
        return jdbcTemplate.query(GET_BOUGHT_TICKETS, new TicketFullInfoMapper(), userId);
    }
}
