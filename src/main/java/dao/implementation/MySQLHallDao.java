package dao.implementation;

import dao.interfaces.HallDAO;
import entities.Hall;
import extractors.HallExtractor;
import forms.HallForm;
import mappers.HallMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MySQLHallDao implements HallDAO {

    private static final int ONE_UPDATED_ROW = 1;
    private static final int ONE_DELETED_ROW = -1;
    private static final String ADD_HALL = "insert into halls (name, rows, places) value (?,?,?) ";
    private static final String GET_HALLS = "select * from halls";
    private static final String GET_HALL_BY_ID = "select * from halls where id = ?";
    private static final String DELETE_HALL = "delete from halls where id = ?";
    private static final String UPDATE_HALL = "update halls set name=?, rows=?, places=? where id=?";
    private static final String GET_HALL_BY_NAME = "select * from halls where name = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean addHall(HallForm hallForm) {
        return jdbcTemplate.update(ADD_HALL, hallForm.getName()
                ,hallForm.getRows(), hallForm.getPlaces()) == ONE_UPDATED_ROW;
    }

    @Override
    public List<Hall> getHalls() {
        return jdbcTemplate.query(GET_HALLS, new HallMapper());
    }

    @Override
    public boolean deleteHall(String hallId) {
        return jdbcTemplate.update(DELETE_HALL, hallId) == ONE_DELETED_ROW;
    }

    @Override
    public Hall getHallById(String hallId) {
        return jdbcTemplate.query(GET_HALL_BY_ID, new HallExtractor(), hallId);
    }

    @Override
    public boolean updateHall(HallForm hallForm, String hallId) {
        return jdbcTemplate.update(UPDATE_HALL, hallForm.getName(),
                hallForm.getRows(), hallForm.getPlaces(), hallId) == ONE_UPDATED_ROW;
    }

    @Override
    public Hall getHallByName(String hallName) {
        return jdbcTemplate.query(GET_HALL_BY_NAME, new HallExtractor(), hallName);
    }
}
