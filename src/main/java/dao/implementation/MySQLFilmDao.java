package dao.implementation;

import dao.interfaces.FilmDAO;
import entities.Film;
import extractors.FilmExtractor;
import forms.FilmForm;
import mappers.FilmMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MySQLFilmDao implements FilmDAO {

    private static final String GET_FILM_BY_NAME = "select * from films where name = ?";
    private static final int ONE_UPDATED_ROW = 1;
    private static final int ONE_DELETED_ROW = -1;
    private static final String ADD_FILM = "insert into films (name, hours, minutes, seconds) value (?,?,?,?) ";
    private static final String GET_FILMS = "select * from films";
    private static final String GET_FILM_BY_ID = "select * from films where id = ?";
    private static final String DELETE_FILM = "delete from films where id = ?";
    private static final String UPDATE_FILM = "update films set name=?, hours=?, minutes=?, seconds=? where id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public boolean addFilm(FilmForm filmForm) {
        return jdbcTemplate.update(ADD_FILM, filmForm.getName(), filmForm.getHours()
                , filmForm.getMinutes(), filmForm.getSeconds()) == 1;
    }

    @Override
    public List<Film> getFilms() {
        return jdbcTemplate.query(GET_FILMS, new FilmMapper());
    }

    @Override
    public boolean deleteFilm(String filmId) {
        return jdbcTemplate.update(DELETE_FILM, filmId) == ONE_DELETED_ROW;
    }

    @Override
    public Film getFilmById(String filmId) {
        return jdbcTemplate.query(GET_FILM_BY_ID, new FilmExtractor(), filmId);
    }

    @Override
    public boolean updateFilm(FilmForm filmForm, String filmId) {
        return jdbcTemplate.update(UPDATE_FILM, filmForm.getName(), filmForm.getHours()
                , filmForm.getMinutes(), filmForm.getSeconds(), filmId) == ONE_UPDATED_ROW;
    }

    @Override
    public Film getFilmByName(String filmName) {
        return jdbcTemplate.query(GET_FILM_BY_NAME, new FilmExtractor(), filmName);
    }
}