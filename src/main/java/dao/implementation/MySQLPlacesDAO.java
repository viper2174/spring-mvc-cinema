package dao.implementation;

import dao.interfaces.PlacesDAO;
import entities.Hall;
import forms.HallForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
public class MySQLPlacesDAO implements PlacesDAO {

    private static final String INSERT_PLACES_BY_HALL_ID = "insert into places (hall_id, row, place) value (?,?,?)";
    private static final String DELETE_PLACES = "delete from places where hall_id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    @Override
    public boolean addPlaces(Hall hall) {
        int insertedPlace = insertPlaces(hall.getId(), hall.getRows(), hall.getPlaces());
        return insertedPlace > 0;
    }

    private int insertPlaces(int id, int rows, int places) {
        int insertedPlace = 0;
        for (int row = 1; row <= rows; row ++) {
            for (int place = 1; place <= places; place ++) {
                insertedPlace += jdbcTemplate.update(INSERT_PLACES_BY_HALL_ID, id, row, place);
            }
        }
        return insertedPlace;
    }

    @Override
    public boolean deletePlaces(String hallId) {
        return jdbcTemplate.update(DELETE_PLACES, hallId) < 0;
    }

    @Override
    public boolean updatePlaces(HallForm hallForm, String hallId) {
        jdbcTemplate.update(DELETE_PLACES, hallId);
        int insertedPlaces = insertPlaces(Integer.valueOf(hallId), hallForm.getRows(), hallForm.getPlaces());
        return insertedPlaces > 0;
    }
}
