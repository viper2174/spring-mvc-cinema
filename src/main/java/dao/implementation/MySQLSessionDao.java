package dao.implementation;

import dao.interfaces.SessionDAO;
import entities.beans.SessionBean;
import entities.beans.SessionInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

@Repository
public class MySQLSessionDao implements SessionDAO {

    private static final String INSERT_SESSION =
            "insert into sessions (film_id, hall_id, start_time) value (?,?,?)";
    private static final String GET_LAST_INSERT_ID = "SELECT last_insert_id()";
    private static final String GET_SESSIONS_BY_HALL = "select s.start_time, f.name, f.hours" +
            ", f.minutes, f.seconds from sessions s inner join films f" +
            " on s.film_id = f.id inner join halls h on s.hall_id = h.id where h.name=?";
    private static final String GET_SESSIONS = "select s.id, s.start_time, f.name, f.hours" +
            ", f.minutes, f.seconds, h.name from sessions s inner join films f" +
            " on s.film_id = f.id inner join halls h on s.hall_id = h.id order by s.start_time asc";
    private static final String GET_SESSION = "select s.id, s.start_time, f.name, f.hours" +
            ", f.minutes, f.seconds, h.name from sessions s inner join films f" +
            " on s.film_id = f.id inner join halls h on s.hall_id = h.id where s.id = ?";
    private static final String DELETE_SESSION = "delete from sessions where id = ?";
    private static final String GET_SESSIONS_BY_FILM_ID = "select s.id, s.start_time, f.name, f.hours" +
            ", f.minutes, f.seconds, h.name from sessions s inner join films f on s.film_id = f.id" +
            " inner join halls h on s.hall_id = h.id where f.id=? order by s.start_time asc";
    private static final String INSERT_TICKET_BY_SESSION =
            "insert into tickets(session_id,place_id,status) value (?,?,'free')";
    private static final String GET_PLACES_BY_HALL_ID = "select id from places where hall_id=?";

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<SessionInfo> getSessionsByHall(String hallName) {
        return jdbcTemplate.query(GET_SESSIONS_BY_HALL, new ResultSetExtractor<List<SessionInfo>>() {
            @Override
            public List<SessionInfo> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<SessionInfo> sessionInfoList = new ArrayList<>();
                while (resultSet.next()) {
                    addSessionToInfoList(resultSet, sessionInfoList);
                }
                return sessionInfoList;
            }
        }, hallName);
    }

    private void addSessionToInfoList(ResultSet resultSet, List<SessionInfo> sessionInfoList) throws SQLException {
        String filmName = resultSet.getString("f.name");
        Timestamp startTime = resultSet.getTimestamp("s.start_time");
        int hours = resultSet.getInt("f.hours");
        int minutes = resultSet.getInt("f.minutes");
        int seconds = resultSet.getInt("f.seconds");
        LocalDateTime endLocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli
                (startTime.getTime()), TimeZone.getDefault().toZoneId());
        endLocalDateTime = endLocalDateTime.plusHours(hours);
        endLocalDateTime = endLocalDateTime.plusMinutes(minutes);
        endLocalDateTime = endLocalDateTime.plusSeconds(seconds);
        SessionInfo sessionInfo = new SessionInfo
                (filmName, startTime.toString(), endLocalDateTime.toString());
        sessionInfoList.add(sessionInfo);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
    public boolean addSession(int filmId, int hallId, LocalDateTime startDate) {
        GeneratedKeyHolder holder = new GeneratedKeyHolder();
        addNewSession (filmId, hallId, startDate, holder);
        long addedSessionId = holder.getKey().longValue();
        List<Integer> placeIds = getAllIdsOfForAddedSession(hallId);
        for (Integer placeId : placeIds) {
            jdbcTemplate.update(INSERT_TICKET_BY_SESSION, addedSessionId, placeId);
        }
        return addedSessionId > 0;
    }

    private void addNewSession(int filmId, int hallId, LocalDateTime startDate, GeneratedKeyHolder holder) {
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement statement = con.prepareStatement(INSERT_SESSION, Statement.RETURN_GENERATED_KEYS);
                statement.setInt(1, filmId);
                statement.setInt(2, hallId);
                statement.setTimestamp(3, Timestamp.valueOf(startDate));
                return statement;
            }
        }, holder);
    }

    private List<Integer> getAllIdsOfForAddedSession(int hallId) {
        List<Integer> placeIds = jdbcTemplate.query(GET_PLACES_BY_HALL_ID, new ResultSetExtractor<List<Integer>>() {
            @Override
            public List<Integer> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<Integer> placeIds = new ArrayList<>();
                while (resultSet.next()) {
                    int placeId = resultSet.getInt("id");
                    placeIds.add(placeId);
                }
                return placeIds;
            }
        }, hallId);
        return placeIds;
    }

    @Override
    public List<SessionBean> getSessions() {
        return jdbcTemplate.query(GET_SESSIONS, new ResultSetExtractor<List<SessionBean>>() {
            @Override
            public List<SessionBean> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<SessionBean> sessionBeanList = new ArrayList<>();
                while (resultSet.next()) {
                    sessionBeanList.add(getSessionBean(resultSet));
                }
                return sessionBeanList;
            }
        });
    }

    private SessionBean getSessionBean(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("s.id");
        String filmName = resultSet.getString("f.name");
        String hallName = resultSet.getString("h.name");
        Timestamp startTime = resultSet.getTimestamp("s.start_time");
        int hours = resultSet.getInt("f.hours");
        int minutes = resultSet.getInt("f.minutes");
        int seconds = resultSet.getInt("f.seconds");
        LocalDateTime endLocalDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli
                (startTime.getTime()), TimeZone.getDefault().toZoneId());
        endLocalDateTime = endLocalDateTime.plusHours(hours);
        endLocalDateTime = endLocalDateTime.plusMinutes(minutes);
        endLocalDateTime = endLocalDateTime.plusSeconds(seconds);
        return new SessionBean
                (id, hallName, filmName, startTime.toString(), endLocalDateTime.toString());
    }

    @Override
    public boolean deleteSession(String sessionId) {
        return jdbcTemplate.update(DELETE_SESSION, sessionId) == -1;
    }

    @Override
    public List<SessionBean> getSessionsByFilmId(String filmId) {
        return jdbcTemplate.query(GET_SESSIONS_BY_FILM_ID, new ResultSetExtractor<List<SessionBean>>() {
            @Override
            public List<SessionBean> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                List<SessionBean> sessionBeanList = new ArrayList<>();
                while (resultSet.next()) {
                    sessionBeanList.add(getSessionBean(resultSet));
                }
                return sessionBeanList;
            }
        }, filmId);
    }

    @Override
    public SessionBean getSession(String sessionId) {
        return jdbcTemplate.query(GET_SESSION, new ResultSetExtractor<SessionBean>() {
            @Override
            public SessionBean extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                if (resultSet.next()) {
                    return getSessionBean(resultSet);
                }
                return null;
            }
        }, sessionId);
    }

}
