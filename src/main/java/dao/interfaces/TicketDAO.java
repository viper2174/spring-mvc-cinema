package dao.interfaces;

import entities.beans.TicketFullInfo;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;

import java.util.List;

public interface TicketDAO {
    List<TicketInfo> getTicketsBySessionId(int sessionId);
    void bookTickets(List<Integer> checked, UserInfo userInfo);
    void buyTickets(List<Integer> checked, UserInfo userInfo);
    List<TicketFullInfo> getBookedTickets(int userId);
    List<TicketFullInfo> getBoughtTickets(int userId);
}
