package dao.interfaces;

import entities.Hall;
import forms.HallForm;

public interface PlacesDAO {
    boolean addPlaces(Hall hall);
    boolean deletePlaces(String hallId);
    boolean updatePlaces(HallForm hallForm, String hallId);
}
