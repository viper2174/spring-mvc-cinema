package dao.interfaces;

import entities.Role;
import entities.User;
import entities.beans.UserInfo;

import java.util.List;

public interface UserDAO {

    UserInfo getUserInfo (String login);
    boolean registerUser (User user);
    User getUserByLogin(String login);
    boolean updateUserPassword(String passwordOld, String passwordNew, String passwordConfirm, UserInfo userInfo);
    List<User> getUsers();
    List<User> getUsersWithoutAdmin();
    boolean changeUserStatus(String login);
    UserInfo getUserInfoByLogin(String login);
}
