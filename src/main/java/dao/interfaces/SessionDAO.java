package dao.interfaces;

import entities.beans.SessionBean;
import entities.beans.SessionInfo;

import java.time.LocalDateTime;
import java.util.List;

public interface SessionDAO {

    List<SessionInfo> getSessionsByHall(String hallName);
    boolean addSession(int filmId, int hallId, LocalDateTime startDate);
    List<SessionBean> getSessions();
    boolean deleteSession(String sessionId);
    List<SessionBean> getSessionsByFilmId(String filmId);
    SessionBean getSession(String sessionId);
}
