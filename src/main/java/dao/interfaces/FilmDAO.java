package dao.interfaces;

import entities.Film;
import forms.FilmForm;

import java.util.List;

public interface FilmDAO {

    boolean addFilm(FilmForm filmForm);
    List<Film> getFilms();
    boolean deleteFilm(String filmId);
    Film getFilmById(String filmId);
    boolean updateFilm(FilmForm filmForm, String filmId);
    Film getFilmByName(String film);
}
