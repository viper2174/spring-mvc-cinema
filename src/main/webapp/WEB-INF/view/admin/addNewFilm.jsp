<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Add film page</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Film name</th>
      <th scope="col">Hours long</th>
      <th scope="col">Minutes long</th>
      <th scope="col">Seconds long</th>
    </tr>
  </thead>
  <tbody>
<tr>
<td>Film name</td>
<td>Hours long</td>
<td>Minutes long</td>
<td>Seconds long</td>
</tr>
<form:form action="${pageContext.servletContext.contextPath}/admin/filmManager/addFilm"
enctype="multipart/form-data" modelAttribute="filmForm" method="post">
<tr>
<td><form:input path="name"/><form:errors path="name" cssClass="error"/></td>
<td><form:input path="hours"/><form:errors path="hours" cssClass="error"/></td>
<td><form:input path="minutes"/><form:errors path="minutes" cssClass="error"/></td>
<td><form:input path="seconds"/><form:errors path="seconds" cssClass="error"/></td>
<td><input type="file" name="file"/></td>
<td><input type="submit" value="addFilm"/></td>
</tr>
</form:form>
</tbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
