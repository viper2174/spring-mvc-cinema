<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Admin menu</h1>
<ul>
    <li role="menuItem"><a href="/cinema/admin/hallManager">Manage halls</a></li>
    <li role="menuItem"><a href="/cinema/admin/filmManager">Manage films</a></li>
    <li role="menuItem"><a href="/cinema/admin/sessionManager">Manage sessions</a></li>
    <li role="menuItem"><a href="/cinema/admin/userManager">Manage users</a></li>
</ul>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
