<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/deleteInfo.js"></script>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Hall Manager</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Hall name</th>
      <th scope="col">Number of rows</th>
      <th scope="col">Number of places</th>
    </tr>
  </thead>
  <tbody>
<c:forEach var="hall" items="${halls}">
<tr id="${hall.id}">
<td>${hall.name}</td>
<td>${hall.rows}</td>
<td>${hall.places}</td>
<td><a class="deleteHall" href="/cinema/admin/hallManager/deleteHall${hall.id}">Delete hall</a></td>
<td><a href="/cinema/admin/hallManager/updateHallPage${hall.id}">Update hall</a></td>
</tr>
</c:forEach>
</tbody>
</table>
<a class="button" href="/cinema/admin/hallManager/addNewHall">Add new hall</a>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
