<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Film Manager</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Film name</th>
      <th scope="col">Hours long</th>
      <th scope="col">Minutes long</th>
      <th scope="col">Seconds long</th>
    </tr>
  </thead>
  <tbody>
<c:forEach var="film" items="${films}">
<tr id="${film.id}">
<td>${film.name}</td>
<td>${film.hours}</td>
<td>${film.minutes}</td>
<td>${film.seconds}</td>
<td><a href="/cinema/admin/filmManager/deleteFilm${film.id}">Delete film</a></td>
<td><a href="/cinema/admin/filmManager/updateFilmPage${film.id}">Update film</a></td>
</tr>
</c:forEach>
</tbody>
</table>
<a class="button" href="/cinema/admin/filmManager/addNewFilm">Add new film</a>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
