<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.1.0/fullcalendar.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.min.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/sessionManager.js"></script>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<center>
<h1>Session Manager</h1>
Choose hall to show schedule</br>
<select onChange="showSchedule(this)">
<c:forEach var="hall" items="${halls}">
<option>${hall.name}</option>
</c:forEach>
</select></br></br></br>
<div class="ui container">
  <div class="ui grid">
    <div class="ui sixteen column">
      <div id="calendar"></div>
    </div>
  </div>
</div></br>
<h1>Registered sessions</h1>
</center>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Hall name</th>
      <th scope="col">Film name</th>
      <th scope="col">Start datetime</th>
      <th scope="col">End datetime</th>
    </tr>
  </thead>
  <tbody>
<c:forEach var="session" items="${sessions}">
<tr id="${session.id}">
<td>${session.hallName}</td>
<td>${session.filmName}</td>
<td>${session.startTime}</td>
<td>${session.endTime}</td>
<td><a href="/cinema/admin/sessionManager/deleteSession${session.id}">Delete session</a></td>
</tr>
</c:forEach>
</tbody>
</table>
<button class="add_session">Add new session</button>
<div class="add_session_form hidden">
<form:form action="${pageContext.servletContext.contextPath}/admin/sessionManager/addSession" modelAttribute="sessionForm" method="post">
choose hall
<form:select path="hall" items="${halls}"/></br>
choose film
<form:select path="film" items="${films}"/></br>
Enter start date
<form:input path="startDate" cssClass="datepicker" required="true"/></br>
Enter end date
<form:input path="endDate" cssClass="datepicker" required="true"/></br>
Enter time of session start
<form:input path="startSession" cssClass="timepicker" required="true"/></br>
<input type="submit" value="Add session"></input>
</form:form>
</div>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
