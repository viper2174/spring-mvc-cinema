<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/changeUserStatus.js"></script>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>User Manager</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Login</th>
      <th scope="col">Name</th>
      <th scope="col">Surname</th>
      <th scope="col">Status</th>
    </tr>
  </thead>
  <tbody>
<c:forEach var="user" items="${users}">
<tr>
<td>${user.login}</td>
<td>${user.name}</td>
<td>${user.surname}</td>
<c:if test="${user.status eq 'blocked'}">
<td><button class="st_ch" id="${user.login}">unblock user</button></td>
</c:if>
<c:if test="${user.status eq 'unblocked'}">
<td><button class="st_ch" id="${user.login}">block user</button></td>
</c:if>
</tr>
</c:forEach>
</tbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
