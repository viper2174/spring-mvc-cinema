<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Update film page</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Film name</th>
      <th scope="col">Hours long</th>
      <th scope="col">Minutes long</th>
      <th scope="col">Seconds long</th>
    </tr>
  </thead>
  <tbody>
<tr>
<td>${updatedFilm.name}</td>
<td>${updatedFilm.hours}</td>
<td>${updatedFilm.minutes}</td>
<td>${updatedFilm.seconds}</td>
<td>Old information</td>
</tr>
<form:form action="${pageContext.servletContext.contextPath}/admin/filmManager/updateFilm${updatedFilm.id}"
modelAttribute="filmUpdateForm" method="post">
<tr>
<td><form:input path="name" value="${updatedFilm.name}"/><form:errors path="name" cssClass="error"/></td>
<td><form:input path="hours" value="${updatedFilm.hours}"/><form:errors path="hours" cssClass="error"/></td>
<td><form:input path="minutes" value="${updatedFilm.minutes}"/><form:errors path="minutes" cssClass="error"/></td>
<td><form:input path="seconds" value="${updatedFilm.seconds}"/><form:errors path="seconds" cssClass="error"/></td>
<td><input type="submit" value="updateFilm"/></td>
</tr>
</form:form>
</tbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
