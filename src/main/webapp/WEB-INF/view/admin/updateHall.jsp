<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Update hall page</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Hall name</th>
      <th scope="col">Number of rows</th>
      <th scope="col">Number of places</th>
    </tr>
  </thead>
  <tbody>
<tr>
<td>${updatedHall.name}</td>
<td>${updatedHall.rows}</td>
<td>${updatedHall.places}</td>
<td>Old information</td>
</tr>
<form:form action="${pageContext.servletContext.contextPath}/admin/hallManager/updateHall${updatedHall.id}"
modelAttribute="hallUpdateForm" method="post">
<tr>
<td><form:input path="name" value="${updatedHall.name}"/><form:errors path="name" cssClass="error"/></td>
<td><form:input path="rows" value="${updatedHall.rows}"/><form:errors path="rows" cssClass="error"/></td>
<td><form:input path="places" value="${updatedHall.places}"/><form:errors path="places" cssClass="error"/></td>
<td><input type="submit" value="updateHall"></input></td>
</tr>
</form:form>
</tbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
