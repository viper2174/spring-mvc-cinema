<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Add hall page</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Hall name</th>
      <th scope="col">Number of rows</th>
      <th scope="col">Number of places</th>
    </tr>
  </thead>
  <tbody>
<form:form action="${pageContext.servletContext.contextPath}/admin/hallManager/addHall" modelAttribute="hallForm" method="post">
<tr>
<td><form:input path="name"/><form:errors path="name" cssClass="error"/></td>
<td><form:input path="rows"/><form:errors path="rows" cssClass="error"/></td>
<td><form:input path="places"/><form:errors path="places" cssClass="error"/></td>
<td><input type="submit" value="addHall"/></td>
</tr>
</form:form>
</tdbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
