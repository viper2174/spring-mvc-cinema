<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Authorization page</h1>
<form:form action="login/check" method="post" modelAttribute="loginForm">
      <table>
          <tr>
              <td>Login:</td>
              <td><form:input path="login"/>
              <form:errors path="login" cssClass="error"></form:errors>
              </td>
          </tr>
          <tr>
              <td>Password:</td>
              <td><form:password path="password"/>
              <form:errors path="password" cssClass="error"></form:errors>
              </td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="sign in" />
              </td>
          </tr>
      </table>
  </form:form>
  <a href="/cinema/registration">Register</a>
  <%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
