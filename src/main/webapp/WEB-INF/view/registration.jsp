<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/registration.css">
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<div class="container">
			<div class="row main">
				<div class="panel-heading">
	               <div class="panel-title text-center">
	               		<h1 class="title">Registration page</h1>
	               		<hr />
	               	</div>
	            </div>
				<div class="main-login main-center">
<form:form action="registration" modelAttribute="registration" method="post">
      <table>
          <tr>
              <td>Login:</td>
              <td><form:input path="login"/>
              <form:errors path="login" cssClass="error"></form:errors></td>
          </tr>
          <tr>
              <td>Password:</td>
              <td><form:password path="password"></form:password>
              <form:errors path="password" cssClass="error"></form:errors></td>
          </tr>
          <tr>
              <td>Name:</td>
              <td><form:input path="name"></form:input>
              <form:errors path="name" cssClass="error"></form:errors></td>
          </tr>
          <tr>
              <td>Surname:</td>
              <td><form:input path="surname"></form:input>
              <form:errors path="surname" cssClass="error"></form:errors></td>
          </tr>
          <tr>
              <td>Email:</td>
              <td><form:input path="email"></form:input>
              <form:errors path="email" cssClass="error"></form:errors></td>
          </tr>
          <tr>
               <td>Birthday:</td>
               <td><form:input path="birthday" placeholder='dd/MM/yyyy'></form:input>
               <form:errors path="birthday" cssClass="error"></form:errors></td>
          </tr>
          <tr>
              <td colspan="2">
                  <input type="submit" value="sign in" />
              </td>
          </tr>
      </table>
  </form:form>
  <%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
