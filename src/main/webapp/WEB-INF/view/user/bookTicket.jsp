<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<center>
<h1>Choose film to book tickets</h1>
<c:forEach var="film" items="${films}">
<a href="${pageContext.request.contextPath}/user/bookTicket/getSessionsByFilm${film.id}" class="btn btn-primary">
<div class="ticket" style="width: 18rem;">
  <img class="card-img-top" height='300px' width='200px' src="bookTicket/showFilmImg${film.name}" alt="Card image cap">
  <div class="card-body">
    <h5 class="card-title">${film.name}</h5>
  </div>
</div>
</a>
</c:forEach>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
