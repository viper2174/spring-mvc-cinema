<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<h1>Menu</h1>
<ul>
    <li role="menuItem"><a href="/cinema/user/contacts">Contacts page</a></li>
    <li role="menuItem"><a href="/cinema/user/bookTicket">Buy you cinema ticket</a></li>
</ul>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
