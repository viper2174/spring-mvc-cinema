<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/ticket.css">
<html>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/passwordChange.js"></script>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</header>
<table class="table">
          <tr>
              <td>Login:</td>
              <td>${userInfo.login}</td>
          </tr>
          <tr>
              <td>Name Surname:</td>
              <td>${userInfo.name} ${userInfo.surname}</td>
          </tr>
          <tr>
              <td>Change password:</td>
              <td> <button class="passwordChange">Update password</button>
              <div class="pswdch_submenu hidden">
              <form:form id="changePassword" action="profile/changePassword" method="post">
              Old password: <input type="password" name="passwordOld" /><br>
              New password: <input type="password" name="passwordNew" /><br>
              New password: <input type="password" name="passwordConfirm" /><br>
              <input type="submit" value="change password"></input>
              </form:form>
              </div>
              </td>
          </tr>
          <tr>
              <td>Avatar: <img src="profile/showAvatar"></td>
              <td>Upload avatar:
                  <c:if test="${not empty userInfo}">
                  <form:form id="updatePassword" action="profile/uploadAvatar" method="post" enctype="multipart/form-data">
                  <input type="file" name="file"></input>
                  <input type="submit" value="submit"></input>
                  </form:form>
                  </c:if>
              </td>
          </tr>
</table>
<details>
<summary>Click to open booked tickets</summary>
Booked tickets
                <c:forEach var="b" items="${bookedTickets}">
                       <div class="container">
                         <section>
                         <c:set var="ticketInfo" value="${b}" scope="session" />
                           <div class="special">Film: ${b.filmName}</div>
                           <div class="special">Start time: ${b.startTime}</div>
                           <div class="special">Hall: ${b.hallName}</div>
                           <div class="special">
                             <div class="seats">
                               <span class="label">row</span><span>${b.row}</span>
                             </div>
                             <img src="/cinema/draw/drawTicket">
                           </div>
                           <div class="special">
                             <div class="seats">
                               <span class="label">seat</span><span>${b.place}</span>
                             </div>
                             Length: ${b.hour}:${b.minute}:${b.second}
                           </div>
                         </section>
                       </div>
                       </br>
                </c:forEach>
</details>
<details>
<summary>Click to open bought tickets</summary>
              Bought tickets
                <c:forEach var="b" items="${boughtTickets}">
                      <div class="container">
                         <section>
                         <c:set var="ticketInfo" value="${b}" scope="session" />
                           <div class="special">Film: ${b.filmName}</div>
                           <div class="special">Start time: ${b.startTime}</div>
                           <div class="special">Hall: ${b.hallName}</div>
                           <div class="special">
                             <div class="seats">
                               <span class="label">row</span><span>${b.row}</span>
                             </div>
                             <img src="/cinema/draw/drawTicket">
                           </div>
                           <div class="special">
                             <div class="seats">
                               <span class="label">seat</span><span>${b.place}</span>
                             </div>
                             Length: ${b.hour}:${b.minute}:${b.second}
                           </div>
                         </section>
                       </div>
                </c:forEach>
                </details>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
