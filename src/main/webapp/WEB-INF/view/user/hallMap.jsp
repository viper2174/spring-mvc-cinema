<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/hallMap.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/hallMap.js"></script>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
Film poster
<img height='300px' width='200px' src="showFilmImg${sessionBean.filmName}"/></br>
Film name
${sessionBean.filmName} </br>
Hall name
${sessionBean.hallName}</br>
Start datetime
${sessionBean.startTime}</br>
End datetime
${sessionBean.endTime}</br>
Max row ${maxRow} </br>

<center>
<c:forEach var="x" begin="1" end="${maxRow}" step="1">
<c:set var="row" value="${maxRow-x+1}" scope="page"></c:set>
<div class="row">
  <c:forEach var="ticket" items="${tickets}">
    <c:if test="${ticket.row == row}">
        <c:if test="${ticket.status == 'free'}">
          <div id=${ticket.id} class="freePlace">
          </div>
        </c:if>
        <c:if test="${ticket.status == 'bought'}">
          <div id=${ticket.id} class="boughtPlace">
          </div>
        </c:if>
        <c:if test="${ticket.status == 'booked'}">
          <div id=${ticket.id} class="bookedPlace">
          </div>
        </c:if>
    </c:if>
  </c:forEach>
</div>
</c:forEach>
<button class="bookTicket" disabled>book tickets</button> </br>
<button class="buyTicket" disabled>buy tickets</button>
</center>
<h1 class="result"/>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
