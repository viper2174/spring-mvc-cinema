<%@ include file="/WEB-INF/view/jspf/head.jspf" %>
<html>
<body>
<header>
<%@ include file="/WEB-INF/view/jspf/header.jspf" %>
</header>
<center>
<h1>Choose session for ${film.name}</h1>
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Start date and time</th>
      <th scope="col">Hall name</th>
      <th>Buy ticket for specified session</th>
    </tr>
  </thead>
  <tbody>
<c:forEach var="session" items="${sessions}">
<tr>
<td>${session.startTime}</td>
<td>${session.hallName}</td>
<td><a href="/cinema/user/bookTicket/openHall${session.id}">Open hall</a></td>
</tr>
</c:forEach>
</tbody>
</table>
<%@ include file="/WEB-INF/view/jspf/footer.jspf" %>
</body>
</html>
