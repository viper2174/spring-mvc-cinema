$(document).on('click','.freePlace', function(){
    $(this).toggleClass('checked');
    if ($('.checked').length){
      $('.buyTicket').prop('disabled', false);
      $('.bookTicket').prop('disabled', false);
    } else {
      $('.buyTicket').prop('disabled', true);
      $('.bookTicket').prop('disabled', true);
    }
});

$(document).on('click','.buyTicket', function(){
    var choosed = [];
    $('.checked').each(function() {
        choosed.push(this.id);
    });
    $.ajax({
    type: "POST",
    url: "/cinema/user/bookTicket/buyTicket",
    data: {
       choosed: choosed
    },
    success: function () {
       $('.result').removeClass('error');
       $('.result').val
       ('tickets have been added to your profile. you can pay and print it');
       location.reload();
    },
    error: function () {
       $('.result').addClass('error');
       $('.result').val('misstake was made. try to buy tickets again');
       location.reload();
    }
    });
});

$(document).on('click','.bookTicket', function(){
    var choosed = [];
    $('.checked').each(function() {
        choosed.push(this.id);
    });
    $.ajax({
    type: "POST",
    url: "/cinema/user/bookTicket/bookTicket",
    data: {
       choosed: choosed
    },
    success: function () {
       $('.result').removeClass('error');
       $('.result').val
       ('tickets have been added to your profile. you can keep them booked until 1 hour before film starts');
       location.reload();
    },
    error: function () {
       $('.result').addClass('error');
       $('.result').val('misstake was made ');
       location.reload();
    }
    });
});