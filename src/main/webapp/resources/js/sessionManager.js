function showSchedule(hall){
        var hallName = hall.value;
        $("#calendar").fullCalendar('removeEventSources');
        $("#calendar").fullCalendar('addEventSource', '/cinema/admin/sessionManager/getFilms?hallName='+ hallName);
        $('#calendar').fullCalendar('refetchEvents');
};

$(document).ready(function() {
        var hallName = $('.hall').val();
        $('#calendar').fullCalendar({
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            height: 600,
            defaultView: 'month',
            defaultDate: $('#calendar').fullCalendar('today'),
            navLinks: true, // can click day/week names to navigate views
            editable: true,
            eventLimit: true, // allow "more" link when too many events
            eventSources: [
                {
                  url: '/cinema/admin/sessionManager/getFilms?hallName='+ hallName,
                }
                          ]
        });

});

$(function() {
   $('.datepicker').datepicker();
   $('.timepicker').timepicker({
       timeFormat: 'HH:mm',
       interval: 5,
       minTime: '0',
       maxTime: '23:59',
       defaultTime: '00:00',
       startTime: '00:00',
       dynamic: true,
       dropdown: true,
       scrollbar: true
   });
});

$(document).on('click','.add_session', function(){
   $(".add_session_form").toggleClass('hidden');
});




