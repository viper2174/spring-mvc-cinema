$(document).on('click','.st_ch', function(){
   var id = this.id;
     $.ajax({
         type: 'POST',
     	 url : window.location.href + "/changeUserStatus",
     	 dataType : 'json',
         data:{ 'userName': id },
     	 success : function(data) {
     	 if (data == 'blocked'){
     	    $('#'+ id).html('unblock user');
     	 } else {
     	    $('#'+ id).html('block user');
     	 }
        }
     });
     location.reload(true);
   });