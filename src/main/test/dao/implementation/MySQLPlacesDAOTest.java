package dao.implementation;

import dao.interfaces.PlacesDAO;
import entities.Hall;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Null;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

public class MySQLPlacesDAOTest {

    private static final String INSERT_PLACES_BY_HALL_ID = "insert into places (hall_id, row, place) value (?,?,?)";
    private static final String DELETE_PLACES = "delete from places where hall_id = ?";

    @InjectMocks
    private MySQLPlacesDAO placesDAO;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void addPlacesShouldReturnTrueWhenTryToAddPlaces() {
        Hall hall = new Hall();
        hall.setId(1);
        hall.setName("1");
        hall.setPlaces(1);
        hall.setRows(1);
        when(jdbcTemplate.update
                (INSERT_PLACES_BY_HALL_ID, 1, 1, 1)).thenReturn(1);
        assertTrue(placesDAO.addPlaces(hall));
    }

    @Test
    public void addPlacesShouldReturnFalseWhenTryToAddPlacesForEmptyHall() {
        Hall hall = new Hall();
        when(jdbcTemplate.update
                (INSERT_PLACES_BY_HALL_ID, 1, 1, 1)).thenReturn(1);
        assertFalse(placesDAO.addPlaces(hall));
    }

    @Test
    public void deletePlacesShouldReturnTrueWhenUpdatedPlacesLessThanOne() {
        String hallId = "1";
        when(jdbcTemplate.update(DELETE_PLACES, hallId)).thenReturn(-1);
        assertTrue(placesDAO.deletePlaces(hallId));
    }

    @Test
    public void deletePlacesShouldReturnFalseWhenUpdatedPlacesMoreThanOne() {
        String hallId = "1";
        when(jdbcTemplate.update(DELETE_PLACES, hallId)).thenReturn(0);
        assertFalse(placesDAO.deletePlaces(hallId));
    }

    @Test
    public void updatePlacesShouldUpdatePlacesWhenPlacesDataIsNew() {
        String hallId = "1";
        Hall hall = new Hall();
        hall.setId(1);
        hall.setName("1");
        hall.setPlaces(1);
        hall.setRows(1);
        when(jdbcTemplate.update
                (INSERT_PLACES_BY_HALL_ID, 1, 1, 1)).thenReturn(1);
        when(jdbcTemplate.update(DELETE_PLACES, hallId)).thenReturn(0);
        assertTrue(placesDAO.addPlaces(hall));
    }

    @Test
    public void updatePlacesShouldNotUpdatePlacesWhenPlacesDataIsOld() {
        Hall hall = new Hall();
        when(jdbcTemplate.update
                (INSERT_PLACES_BY_HALL_ID, 1, 1, 1)).thenReturn(0);
        when(jdbcTemplate.update(DELETE_PLACES, "0")).thenReturn(0);
        assertFalse(placesDAO.addPlaces(hall));
    }
}