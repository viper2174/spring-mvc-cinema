package services.implementation;

import dao.interfaces.FilmDAO;
import dao.interfaces.HallDAO;
import dao.interfaces.SessionDAO;
import entities.Film;
import entities.Hall;
import entities.beans.UserInfo;
import forms.SessionForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import services.interfaces.PictureService;
import services.interfaces.SessionService;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class SessionServiceImplTest {

    @InjectMocks
    private SessionServiceImpl sessionService;

    @Mock
    private SessionDAO sessionDAO;

    @Mock
    private HallDAO hallDao;

    @Mock
    private FilmDAO filmDAO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldAddSessionWhenEndDateIsAfterStartDate() {
        when(filmDAO.getFilmByName("test")).thenReturn
                (new Film(1,"",1,1,1));
        when(hallDao.getHallByName("test")).thenReturn
                (new Hall(1,"", 0,0));
        when(sessionDAO.addSession(anyInt(), anyInt(), any())).thenReturn(true);
        SessionForm sessionForm = new SessionForm();
        sessionForm.setStartDate("11/11/2011");
        sessionForm.setEndDate("11/12/2011");
        sessionForm.setStartSession("12:33");
        sessionForm.setHall("test");
        sessionForm.setFilm("test");
        assertTrue(sessionService.addSession(sessionForm));
    }

    @Test
    public void getEndDateTest() {
        when(filmDAO.getFilmByName("test")).thenReturn
                (new Film(1,"",1,1,1));
        when(hallDao.getHallByName("test")).thenReturn
                (new Hall(1,"", 0,0));
        when(sessionDAO.addSession(anyInt(), anyInt(), any())).thenReturn(true);
        SessionForm sessionForm = new SessionForm();
        sessionForm.setStartDate("11/12/2011");
        sessionForm.setEndDate("11/11/2011");
        sessionForm.setStartSession("12:33");
        sessionForm.setHall("test");
        sessionForm.setFilm("test");
        assertFalse(sessionService.addSession(sessionForm));

    }

}