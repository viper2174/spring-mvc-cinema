package services.implementation;

import entities.beans.TicketInfo;
import org.junit.Assert;
import org.junit.Test;
import services.interfaces.TicketService;

import java.util.ArrayList;
import java.util.List;

public class TicketServiceImplTest {

    private static final String FREE = "free";

    @Test
    public void shouldChangeTicketOrderWhenTryToSortTickets (){
        TicketService ticketService = new TicketServiceImpl();
        List<TicketInfo> list = new ArrayList<>();
        TicketInfo ticket11 = new TicketInfo(1,1,1,FREE);
        TicketInfo ticket12 = new TicketInfo(1,1,2,FREE);
        TicketInfo ticket21 = new TicketInfo(1,2,1,FREE);
        TicketInfo ticket22 = new TicketInfo(1,2,2,FREE);
        list.add(ticket12);
        list.add(ticket21);
        list.add(ticket22);
        list.add(ticket11);
        List<TicketInfo> sortedListExpected = new ArrayList<>();
        sortedListExpected.add(ticket12);
        sortedListExpected.add(ticket11);
        sortedListExpected.add(ticket22);
        sortedListExpected.add(ticket21);
        ticketService.sortTickets(list);
        Assert.assertEquals(sortedListExpected, list);
    }

}