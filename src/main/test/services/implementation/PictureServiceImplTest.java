package services.implementation;

import entities.beans.UserInfo;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import services.interfaces.PictureService;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Date;

import static org.junit.Assert.*;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class PictureServiceImplTest {

    private static final String TEST_IMG_PATH = "src/main/webapp/resources/img/avatars/default.png";
    private static final int TEST_INT = 1;
    private static final String TEST_STRING = "test";
    private static final Date TEST_DATE = Date.valueOf("2001-01-01");
    private static final String TEST_IMG_FINAL_PATH = "src/main/webapp/resources/img/avatars/";
    private static final String TEST_POSTER_FINAL_PATH = "src/main/webapp/resources/img/films/";
    private static final String DEFAULT_AVATAR_PATH = "src/main/webapp/resources/img/questionMark.png";
    private static final String ADMIN_AVATAR = "src/main/webapp/resources/img/avatars/admin.png";
    private static final String TEST_FILM_PATH = "src/main/webapp/resources/img/films/south park.png";
    private static final String DEFAULT_POSTER = "src/main/webapp/resources/img/questionMark.png";
    private static final String TEST_POSTER = "src/main/webapp/resources/img/films/south park.png";
    private static final String FREE_ICON_PATH = "src/main/webapp/resources/img/grey.png";
    private static final String BOUGHT_ICON_PATH = "src/main/webapp/resources/img/red.png";

    @Before
    public void setUp() throws IOException {
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        Files.deleteIfExists(Paths.get(TEST_IMG_FINAL_PATH + userInfo.getLogin() + ".png"));
        Files.deleteIfExists(Paths.get(TEST_POSTER_FINAL_PATH + "test.png"));
    }


    @Test
    @WithMockUser (authorities = "user")
    public void shouldUploadAvatarThenIncomingParamsAreValid() throws IOException {
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        Files.deleteIfExists(Paths.get(TEST_IMG_FINAL_PATH + userInfo.getLogin() + ".png"));
        PictureService pictureService = new PictureServiceImpl();
        FileInputStream inputFile = new FileInputStream(TEST_IMG_PATH);
        Assert.assertNotNull(inputFile);
        MockMultipartFile file = new MockMultipartFile
                ("file", "default", "multipart/form-data", inputFile);
        pictureService.uploadAvatar(file, userInfo);
        byte [] avatar = Files.readAllBytes
                (Paths.get(TEST_IMG_FINAL_PATH + userInfo.getLogin() + ".png"));
        assertTrue (file.getBytes().length == avatar.length);
    }

    @Test
    @WithMockUser (authorities = "user")
    public void shouldUploadFilmPosterWhenIncomingParamsAreValid() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        FileInputStream inputFile = new FileInputStream(TEST_FILM_PATH);
        Assert.assertNotNull(inputFile);
        MockMultipartFile file = new MockMultipartFile
                ("file", "1", "multipart/form-data", inputFile);
        pictureService.uploadFilmImg(file, "test");
        byte [] poster = Files.readAllBytes
                (Paths.get(TEST_POSTER_FINAL_PATH + "test.png"));
        assertTrue (file.getBytes().length == poster.length);
    }

    @Test
    public void shouldNotUploadAvatarWhenFileDoesNotExist() {
        PictureService pictureService = new PictureServiceImpl();
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        pictureService.uploadAvatar(null, userInfo);
        assertFalse (Files.exists(Paths.get(TEST_IMG_FINAL_PATH + userInfo.getLogin() + ".png")));
    }

    @Test
    public void shouldReturnNullWhenUserInfoIsNull() {
        assertEquals(null, new PictureServiceImpl().getAvatar(null));
    }

    @Test
    public void shouldGetUserAvatarWhenDataIsValid() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        UserInfo userInfo = new UserInfo(TEST_INT,"admin",TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        assertEquals(Files.readAllBytes(new File(ADMIN_AVATAR).toPath()).length,
                pictureService.getAvatar(userInfo).length);
    }

    @Test
    public void shouldGetDefaultAvatarWhenDataIsInvalid() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        assertEquals(Files.readAllBytes(new File(DEFAULT_AVATAR_PATH).toPath()).length,
                pictureService.getAvatar(userInfo).length);
    }

    @Test
    public void shouldGetFilmPathWhenFilmNameIsValid() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        assertEquals(pictureService.getFilmPicture("south park").length,
                Files.readAllBytes(new File(TEST_POSTER).toPath()).length);
    }

    @Test
    public void shouldReturnDefaultPosterWhenFilmNameIsInvalid() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        assertEquals(pictureService.getFilmPicture("").length,
                Files.readAllBytes(new File(DEFAULT_POSTER).toPath()).length);
    }

    @Test
    public void shouldReturnFreeChairImage() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        assertEquals(pictureService.getChairColorForUser("free").length,
                Files.readAllBytes(new File(FREE_ICON_PATH).toPath()).length);
    }

    @Test
    public void shouldReturnBoughtChairImage() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        assertEquals(pictureService.getChairColorForUser("bought").length,
                Files.readAllBytes(new File(BOUGHT_ICON_PATH).toPath()).length);
    }

    @Test
    public void shouldReturnBookedChairImage() throws IOException {
        PictureService pictureService = new PictureServiceImpl();
        assertEquals(pictureService.getChairColorForUser("booked").length,
                Files.readAllBytes(new File(BOUGHT_ICON_PATH).toPath()).length);
    }
}