package services.implementation;
import controllers.AdminController;
import dao.interfaces.HallDAO;
import dao.interfaces.PlacesDAO;
import entities.Hall;
import forms.HallForm;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.mockito.Mockito.when;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class HallServiceImplTest {

    private static final String TEST_ID = "1";
    @InjectMocks
    private HallServiceImpl hallService;

    @Mock
    private HallDAO hallDAO;

    @Mock
    private PlacesDAO placesDAO;

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(hallService).build();
    }

    @Test
    public void shouldAddHallToDataBaseWhenDataIsValid(){
        HallForm hallForm = new HallForm();
        Hall hall = new Hall();
        when(hallDAO.addHall(hallForm)).thenReturn(true);
        when(hallDAO.getHallByName(hallForm.getName())).thenReturn(hall);
        when(placesDAO.addPlaces(hall)).thenReturn(true);
        Assert.assertEquals(true, hallService.addHall(hallForm));
    }

    @Test
    public void shouldNotAddHallToDataBaseWhenDataIsInvalid(){
        HallForm hallForm = new HallForm();
        Hall hall = new Hall();
        when(hallDAO.addHall(hallForm)).thenReturn(false);
        when(hallDAO.getHallByName(hallForm.getName())).thenReturn(hall);
        when(placesDAO.addPlaces(hall)).thenReturn(true);
        Assert.assertEquals(false, hallService.addHall(hallForm));
    }

    @Test
    public void shouldDeleteHallFromDatabaseWhenDataIsValid(){
        when(hallDAO.deleteHall(TEST_ID)).thenReturn(true);
        when(placesDAO.deletePlaces(TEST_ID)).thenReturn(true);
        Assert.assertEquals(true, hallService.deleteHall(TEST_ID));
    }

    @Test
    public void shouldNotDeleteHallFromDatabaseWhenDataIsInvalid(){
        when(hallDAO.deleteHall(TEST_ID)).thenReturn(false);
        when(placesDAO.deletePlaces(TEST_ID)).thenReturn(false);
        Assert.assertEquals(false, hallService.deleteHall(TEST_ID));
    }

    @Test
    public void shouldUpdateHallToDataBaseWhenDataIsValid(){
        HallForm hallForm = new HallForm();
        when(hallDAO.updateHall(hallForm, TEST_ID)).thenReturn(true);
        when(placesDAO.updatePlaces(hallForm, TEST_ID)).thenReturn(true);
        Assert.assertEquals(true, hallService.updateHall(hallForm, TEST_ID));
    }

    @Test
    public void shouldNotUpdateHallToDataBaseWhenDataIsInvalid(){
        HallForm hallForm = new HallForm();
        when(hallDAO.updateHall(hallForm, TEST_ID)).thenReturn(false);
        when(placesDAO.updatePlaces(hallForm, TEST_ID)).thenReturn(true);
        Assert.assertEquals(false, hallService.updateHall(hallForm, TEST_ID));
    }
}