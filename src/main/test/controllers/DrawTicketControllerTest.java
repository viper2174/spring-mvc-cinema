package controllers;

import entities.beans.TicketFullInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import services.interfaces.PictureService;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import static org.mockito.Mockito.doCallRealMethod;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class DrawTicketControllerTest{

    private static final String TEST_STRING = "testString";
    private static final int TEST_INT = 1;
    private static final int WIDTH = 100;
    private static final int HEIGHT = 50;

    private MockMvc mockMvc;

    @InjectMocks
    private DrawTicketController drawTicketController;

    @Mock
    private PictureService pictureService;

    @Mock
    private HttpServletResponse httpServletResponse;

    @Mock
    private ServletOutputStream servletOutputStream;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(drawTicketController).build();
    }

    @Test
    public void shouldReturnQRCodeWhenTryToDrawTicket() throws Exception {
        byte [] zxHelper = new byte[1];
        TicketFullInfo ticketFullInfo = new TicketFullInfo(TEST_STRING, TEST_INT, TEST_INT, TEST_INT,
                TEST_STRING, TEST_INT, TEST_INT, TEST_STRING, TEST_STRING,TEST_STRING,TEST_STRING);
        when(pictureService.getZXhelper(ticketFullInfo, WIDTH, HEIGHT)).thenReturn(zxHelper);
        mockMvc.perform(get("/draw/drawTicket")
                .sessionAttr("ticketInfo", ticketFullInfo))
                .andReturn();
    }
}