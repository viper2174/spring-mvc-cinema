package controllers;

import config.UserInfoSession;
import entities.Film;
import entities.beans.SessionBean;
import entities.beans.TicketInfo;
import entities.beans.UserInfo;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import services.interfaces.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class BookTicketControllerTest {

    private static final String TEST_STRING = "testString";
    private static final int TEST_INT = 1;
    private static final int FIRST_ROW = 1;
    private static final int SECOND_ROW = 2;
    private static final Date TEST_DATE = Date.valueOf("2001-01-01");

    @InjectMocks
    private BookTicketController bookTicketController;

    private MockMvc mockMvc;

    @Mock
    private FilmService filmService;

    @Mock
    private PictureService pictureService;

    @Mock
    private SessionService sessionService;

    @Mock
    private TicketService ticketService;

    @Mock
    private UserInfoSession userInfoSession;

    @Mock
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(bookTicketController).build();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldForwardBookTicketPageWhenTryToOpenBookTicketPage () throws Exception {
        List<Film> filmList = new ArrayList<>();
        filmList.add(new Film(TEST_INT, TEST_STRING,TEST_INT,TEST_INT,TEST_INT));
        when(filmService.getFilms()).thenReturn(filmList);
        mockMvc.perform(get("/user/bookTicket"))
                .andExpect(model().attribute("films", filmService.getFilms()))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("user/bookTicket"))
                .andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldReturnFilmImageWhenTryToGetFilmPicture() throws Exception {
        byte [] testImage = new byte[0];
        when(pictureService.getFilmPicture(TEST_STRING)).thenReturn(testImage);
        mockMvc.perform(get("/user/bookTicket/showFilmImg{filmName}", TEST_STRING))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldReturnListOfSessionWhenTryToOpenShowSessionsPage() throws Exception {
        List<SessionBean> testSessions = new ArrayList<>();
        testSessions.add(new SessionBean(TEST_INT,TEST_STRING, TEST_STRING, TEST_STRING, TEST_STRING));
        Film testFilm = new Film();
        when(sessionService.getSessionsByFilmId(TEST_STRING)).thenReturn(testSessions);
        when(filmService.getFilmById(TEST_STRING)).thenReturn(testFilm);
        mockMvc.perform (get("/user/bookTicket/getSessionsByFilm{filmId}", TEST_STRING))
                .andExpect(model().attribute("film", filmService.getFilmById(TEST_STRING)))
                .andExpect(model().attribute("sessions", sessionService.getSessionsByFilmId(TEST_STRING)))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("user/showSessions"))
                .andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldAddTicketsWhenTryToOpenHallMap () throws Exception {
        SessionBean testSessionBean = new SessionBean(TEST_INT, TEST_STRING, TEST_STRING, TEST_STRING, TEST_STRING);
        List<TicketInfo> ticketInfoList = new ArrayList<>();
        ticketInfoList.add(new TicketInfo(TEST_INT, SECOND_ROW, TEST_INT, TEST_STRING));
        ticketInfoList.add(new TicketInfo(TEST_INT, FIRST_ROW, TEST_INT, TEST_STRING));
        when(sessionService.getSession(String.valueOf(TEST_INT))).thenReturn(testSessionBean);
        when(ticketService.getTicketsBySessionId(TEST_INT)).thenReturn(ticketInfoList);
        mockMvc.perform (get("/user/bookTicket/openHall{sessionId}", TEST_INT))
                .andExpect(model().attribute("tickets", ticketInfoList))
                .andExpect(model().attribute("sessionBean", testSessionBean))
                .andExpect(model().attribute("maxRow", SECOND_ROW))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("user/hallMap"))
                .andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldGetRequiredColourWhenTryToGetChairPicture () throws Exception {
        byte [] testImage = new byte[0];
        when(pictureService.getChairColorForUser(TEST_STRING)).thenReturn(testImage);
        mockMvc.perform(get("/user/bookTicket/determineChairColorForUser{status}", TEST_STRING))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldBookTicketWhenTryToBookTicket () throws Exception {
        List<Integer> testChecked = new ArrayList<>();
        testChecked.add(1);
        testChecked.add(2);
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
        TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        when (userInfoSession.getUserFromSession(userService)).thenReturn(userInfo);
        doNothing().when(ticketService).bookTickets(testChecked, userInfo);
        mockMvc.perform(post("/user/bookTicket/bookTicket")
                .param("choosed[]", "1", "2"))
                .andDo(print())
                .andExpect(forwardedUrl("/profile"))
                .andReturn();
    }

    @Test
    @WithMockUser(authorities = "user")
    public void shouldBuyTicketWhenTryToBuyTicket () throws Exception {
        List<Integer> testChecked = new ArrayList<>();
        testChecked.add(1);
        testChecked.add(2);
        UserInfo userInfo = new UserInfo(TEST_INT,TEST_STRING,TEST_STRING,TEST_STRING,TEST_STRING,
                TEST_DATE,TEST_STRING,TEST_STRING,TEST_STRING);
        when (userInfoSession.getUserFromSession(userService)).thenReturn(userInfo);
        doNothing().when(ticketService).bookTickets(testChecked, userInfo);
        mockMvc.perform(post("/user/bookTicket/buyTicket")
                .param("choosed[]", "1", "2"))
                .andDo(print())
                .andExpect(forwardedUrl("/profile"))
                .andReturn();
    }

}