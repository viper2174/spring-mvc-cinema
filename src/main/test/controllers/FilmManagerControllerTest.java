package controllers;

import entities.Film;
import forms.FilmForm;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.multipart.MultipartFile;
import services.interfaces.FilmService;
import services.interfaces.PictureService;
import services.interfaces.UserService;

import javax.validation.ConstraintViolation;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ContextConfiguration("file:src/main/webapp/WEB-INF/app-root-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
public class FilmManagerControllerTest{

    @InjectMocks
    private FilmManagerController filmManagerController;

    private MockMvc mockMvc;

    @Mock
    private FilmService filmService;

    @Mock
    private PictureService pictureService;

    @Mock
    private BindingResult bindingResult;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(filmManagerController).build();
    }

    @Test
    @WithMockUser(authorities = "admin")
    public void shouldGetFilmManagerPageWhenTryToOpenFilmManagerMenu() throws Exception {
        List<Film> films = new ArrayList<>();
        films.add(new Film());
        when(filmService.getFilms()).thenReturn(films);
        mockMvc.perform(get("/admin/filmManager"))
                .andExpect(model().attribute("films", films))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("admin/filmManager"));
    }

    @Test
    @WithMockUser(authorities = "admin")
    public void shouldAddFilmFormInSessionWhenTryToOpenFilmBuilderPage() throws Exception {
        FilmForm filmForm = new FilmForm();
        filmForm.setName("name");
        filmForm.setHours(1);
        filmForm.setMinutes(1);
        filmForm.setSeconds(1);
        mockMvc.perform(get("/admin/filmManager/addNewFilm")
                .flashAttr("filmForm", filmForm))
                .andExpect(model().attributeExists("filmForm"))
                .andExpect(status().isOk())
                .andExpect(forwardedUrl("admin/addNewFilm"));
    }

    @Test
    @WithMockUser(authorities = "admin")
    public void shouldRedirectOnFilmManagerPageWhenTryToUploadFilmPosterWithValidData() throws Exception {
        MockMultipartFile file = new MockMultipartFile
                ("test", "test.png", MediaType.MULTIPART_FORM_DATA_VALUE, "some xml".getBytes());
        mockMvc.perform(MockMvcRequestBuilders.multipart("/admin/filmManager/addFilm")
                .file("file", file.getBytes())
                .param("name", "right")
                .param("hours", "1")
                .param("minutes", "1")
                .param("seconds", "1")
                .contentType(MediaType.MULTIPART_FORM_DATA))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admin/filmManager"));
    }

}