drop schema if exists cinema;
create database cinema;
use cinema;

create table roles (
  id int UNIQUE not null,
  name varchar(10) UNIQUE not null,
  primary key (id)
  );

insert into roles (id,name)
values (1, 'admin') , (2, 'user');

create table users (
login varchar(20) UNIQUE not null,
password varchar (1000) not null,
name varchar (20) not null,
surname varchar (30) not null,
birthday date not null,
email varchar (30) not null,
role_id int NOT NULL,
status varchar (10) not null,
enabled boolean default true,
FOREIGN KEY (role_id) REFERENCES roles(id) ON DELETE CASCADE ON UPDATE RESTRICT,
primary key (login)
);

insert into users (login,password, name, surname, birthday, email, role_id, status)
values ('admin','$2a$10$I/qSGnD3w85l6yMdhk.bleN.HWfhXgzDKF7tVJN3qCOzeG/6mG6O.', 'admin', 'adminov', '1995-11-11', 'admin@gmail.com', 1, 'unblocked'),('user','$2a$10$luG1jQkPcWLO8uWeO..vA.Ramba/HcN1b5PAeJVP9XQCpjMST/o.K', 'user', 'userov', '2000-11-11','user@gmail.com', 2, 'unblocked');

create table halls (
  id int UNIQUE not null auto_increment,
  name varchar(10) UNIQUE not null,
  rows int not null,
  places int not null,
  primary key (id)
  );

insert into halls (name,rows,places)
values ('red', 10, 10) , ('blue', 5 , 15), ('vip', 5, 5);

create table places (
  id int unique not null auto_increment,
  hall_id int not null,
  raw int not null,
  place int not null,
  foreign key (hall_id) references halls(id) on delete cascade on update restrict,
  primary key (id)
  );

create table films (
  id int UNIQUE not null auto_increment,
  name varchar(200) UNIQUE not null,
  hours int not null,
  minutes int not null,
  seconds int not null,
  primary key (id)
  );

insert into films (name,hours,minutes,seconds)
values ('green mile', 2, 40, 30) , ('ice age', 2, 10, 15);

create table sessions (
  id int UNIQUE not null auto_increment,
  film_id int not null,
  hall_id int not null,
  start_time datetime,
  FOREIGN KEY (film_id) REFERENCES films(id) ON DELETE CASCADE ON UPDATE RESTRICT,
  FOREIGN KEY (hall_id) REFERENCES halls(id) ON DELETE CASCADE ON UPDATE RESTRICT,
  primary key (id)
  );

create table tickets (
id int UNIQUE not null auto_increment,
session_id int not null,
place_id int not null,
status varchar (10) not null UNIQUE,
FOREIGN KEY (session_id) REFERENCES sessions(id) ON DELETE CASCADE ON UPDATE RESTRICT,
FOREIGN KEY (place_id) REFERENCES places(id) ON DELETE CASCADE ON UPDATE RESTRICT,
primary key (id)
);

create table booked (
id int UNIQUE not null auto_increment,
ticket_id int not null,
user_id int not null,
FOREIGN KEY (ticket_id) REFERENCES ticket(id) ON DELETE CASCADE ON UPDATE RESTRICT,
FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE RESTRICT,
primary key (id)
);

create table bought (
id int UNIQUE not null auto_increment,
ticket_id int not null,
user_id int not null,
FOREIGN KEY (ticket_id) REFERENCES tickets(id) ON DELETE CASCADE ON UPDATE RESTRICT,
FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE ON UPDATE RESTRICT,
primary key (id)
);